export class User {
    id: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    title?: string;
    password?: string;
    phone?: string;
    public fullName() {
        return this.firstName + ' ' + this.lastName;
    }
}
