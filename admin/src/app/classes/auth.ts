export class AuthToken {
  token: string;
  id: string;
  constructor( token: string, id: string ) {
    this.token = token;
    this.id = id;
  }
}

export class MagicLogin extends AuthToken {
  redirect: string;
  constructor(token: string, userid: string, redirect: string) {
    super(token, userid);
    this.token = token,
    this.id = userid,
    this.redirect = redirect;
  }

  public reduceToAuthToken(magicLogin: MagicLogin){
    return new AuthToken(magicLogin.token, magicLogin.id);
  }
}