import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class TermsAndConditionsService {

  constructor(private http: HttpClient) {
  }

  acceptTerms() {
    return this.http.post(environment.apiUrl + '/termsandcondition/user/', null);
  }

  getCurrentUser() {
    return this.http.get(environment.apiUrl + '/termsandcondition/user/');
  }

  acceptOrganisationTerms() {
    return this.http.post(environment.apiUrl + '/termsandcondition/organisation/', null);
  }

  getCurrentOrganisationTerms() {
    return this.http.get(environment.apiUrl + '/termsandcondition/organisation/');
  }

}
