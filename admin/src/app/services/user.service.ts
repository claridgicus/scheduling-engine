import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  getCurrentUser() {
    return this.http.get(environment.apiUrl + '/user/');
  }

  getUserById(userId: string) {
    return this.http.get(environment.apiUrl + '/user/role/' + userId);
  }

  getSimpleUserById(userId: string) {
    return this.http.get(environment.apiUrl + '/user/' + userId);
  }

  updateCurrentUser(user: any) {
    return this.http.post(environment.apiUrl + '/user/update', user);
  }

  updateUser(user: any) {
    return this.http.post(environment.apiUrl + '/user/update/user', user );
  }

  resendActivationEmail(email: string){
    return this.http.post(environment.apiUrl + '/user/resend', {email});
  }
}
