// tslint:disable:object-literal-sort-keys
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// tslint:disable-next-line:max-line-length
import {
  ExpiredComponent, FullscreenLayoutComponent, ClientDashboardComponent, LoginComponent, ResetComponent, SetComponent, SignupComponent, VerifyComponent, WindowLayoutComponent
} from './components';
import { AuthGuard } from './guards';

const routes: Routes = [
  {
    path: '', component: FullscreenLayoutComponent,
    children: [
      { path: 'set', component: SetComponent, canActivate: [AuthGuard] },
      { path: 'verify', component: VerifyComponent },
      { path: 'expired', component: ExpiredComponent },
      { path: 'reset', component: ResetComponent },
      { path: 'login', component: LoginComponent },
      { path: 'signup', component: SignupComponent },
    ]
  },
  {
    path: '', component: WindowLayoutComponent, canActivate: [AuthGuard],
    children: [
      { path: 'dashboard', component: ClientDashboardComponent },
    ]
  }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})

export class AppRoutingModule { }
