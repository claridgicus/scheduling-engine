import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthToken, MagicLogin } from '../../../classes';
import { AuthenticationService } from '../../../services';

@Component({
  selector: 'menu-user',
  styleUrls: ['./user.component.scss'],
  templateUrl: './user.component.html'
})

export class UserMenuComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}