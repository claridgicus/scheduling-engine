import {APP_BASE_HREF} from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatCheckboxModule, MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AuthLayoutComponent, ExpiredComponent, FullscreenLayoutComponent, LoginComponent, ResetComponent, SetComponent, SignupComponent, VerifyComponent } from '../..';
import { AppRoutingModule } from '../../../app-routing.module';
import { WindowLayoutComponent } from './window-layout.component';

describe('WindowLayoutComponent', () => {
  let component: WindowLayoutComponent;
  let fixture: ComponentFixture<WindowLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WindowLayoutComponent, AuthLayoutComponent, SetComponent, ResetComponent, VerifyComponent, ExpiredComponent, FullscreenLayoutComponent, LoginComponent, SignupComponent ],
      imports: [ BrowserAnimationsModule, RouterModule, AppRoutingModule, FormsModule, MatButtonModule, MatCheckboxModule, MatInputModule],
      providers: [{provide: APP_BASE_HREF, useValue: '/'}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WindowLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
