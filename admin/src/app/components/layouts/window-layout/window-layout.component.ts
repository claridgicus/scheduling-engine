import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-window-layout',
  templateUrl: './window-layout.component.html',
  styleUrls: ['./window-layout.component.scss']
})
export class WindowLayoutComponent implements OnInit {
  currentYear: string;
  termsandconditions: boolean;

  constructor() {
    this.currentYear = new Date().getFullYear().toString();
  }

  ngOnInit() {
  }
}
