import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../../classes/user';
import { AuthenticationService } from '../../../services';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  user: User;
  error: string;
  success: string;

  constructor(private authService: AuthenticationService, private router: Router) {
    this.user = new User();
  }

  ngOnInit() {
  }

  createAccount() {
    const email = this.user.email;
    this.authService.createUser(this.user).subscribe(
      accountCreated => {
        console.log(email);
        this.router.navigate(['/verify'], { queryParams: {email}}); });
  }
}
