import { APP_BASE_HREF } from '@angular/common';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MatButtonModule, MatCheckboxModule, MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AuthLayoutComponent, ExpiredComponent, FullscreenLayoutComponent, LoginComponent, ResetComponent, SetComponent, SignupComponent, VerifyComponent } from '../..';
import { AppRoutingModule } from '../../../app-routing.module';
import { AuthenticationService } from '../../../services';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthLayoutComponent, SetComponent, ResetComponent, VerifyComponent, FullscreenLayoutComponent, LoginComponent, SignupComponent, ExpiredComponent ],
      imports: [ BrowserAnimationsModule, RouterModule, AppRoutingModule, FormsModule,  MatButtonModule, MatCheckboxModule, MatInputModule],
      providers: [AuthenticationService, {provide: APP_BASE_HREF, useValue: '/'}, HttpClient, HttpHandler]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
