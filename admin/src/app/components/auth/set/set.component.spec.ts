import { APP_BASE_HREF } from '@angular/common';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MatButtonModule, MatCheckboxModule, MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AuthLayoutComponent, ExpiredComponent, FullscreenLayoutComponent, LoginComponent, ResetComponent, SetComponent, SignupComponent, VerifyComponent } from '../..';
import { AppRoutingModule } from '../../../app-routing.module';
import { AuthenticationService, UserService } from '../../../services';

describe('SetComponent', () => {
  let component: SetComponent;
  let fixture: ComponentFixture<SetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthLayoutComponent, SetComponent, ResetComponent, VerifyComponent, FullscreenLayoutComponent, LoginComponent, SignupComponent, ExpiredComponent ],
      imports: [ BrowserAnimationsModule, RouterModule, AppRoutingModule, FormsModule,  MatButtonModule, MatCheckboxModule, MatInputModule],
      providers: [AuthenticationService, UserService, {provide: APP_BASE_HREF, useValue: '/'}, HttpClient, HttpHandler]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
