import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services';
import { UserService } from '../../../services';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})

export class ResetComponent implements OnInit {
  email: string;
  error: string;
  success: string;
  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

  resetPassword() {
    this.authService.resetPassword(this.email).subscribe(
      passwordReset => { this.router.navigate(['/verify']); });
  }
}
