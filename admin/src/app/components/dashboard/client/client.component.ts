import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
@Component({
  selector: 'dashboard-client',
  styleUrls: ['./client.component.scss'],
  templateUrl: './client.component.html'
})
export class ClientDashboardComponent implements OnInit {
  currentYear: string;
  user: any;
  selection: string;

  constructor() {
    this.selection = 'make';
    this.currentYear = new Date().getFullYear().toString();
    this.user = {
      firstName: 'James',
      lastName: 'Claridge'
    };
  }

  ngOnInit() {

  }
}
