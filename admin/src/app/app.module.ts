import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MatButtonModule, MatCheckboxModule, MatInputModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  ClientDashboardComponent,
  ExpiredComponent,
  FullscreenLayoutComponent,
  LoginComponent,
  PrimaryMenuComponent,
  ResetComponent,
  SetComponent,
  SignupComponent,
  UserMenuComponent,
  UserToggleComponent,
  VerifyComponent,
  WindowLayoutComponent,
  ServiceSelectionComponent,
  ListBookingComponent
 } from './components';
import { AuthGuard } from './guards';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { AuthenticationService,
  UserService } from './services';

@NgModule({
    bootstrap: [
      AppComponent
    ],
    declarations: [
      ExpiredComponent,
      FullscreenLayoutComponent,
      WindowLayoutComponent,
      LoginComponent,
      SetComponent,
      AppComponent,
      ResetComponent,
      SignupComponent,
      VerifyComponent,
      PrimaryMenuComponent,
      UserToggleComponent,
      ClientDashboardComponent,
      UserMenuComponent,
      ServiceSelectionComponent,
      ListBookingComponent
    ],
    imports: [
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      AppRoutingModule,
      RouterModule,
      HttpModule,
      BrowserAnimationsModule,
      HttpClientModule,
      NgHttpLoaderModule,
      BrowserAnimationsModule,
      MatButtonModule,
      MatCheckboxModule,
      MatInputModule
    ],
    providers: [{
      multi: true,
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      },
      AuthGuard,
      // UserTypeGuard,
      AuthenticationService,
      UserService
    ],
    schemas: [NO_ERRORS_SCHEMA],
  })
  export class AppModule {}
