import { BadRequestException, Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Req, UseGuards, ValidationPipe} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ScheduleService } from 'services/schedule.service';
import {BookingItemDto, ContractorScheduleDto, ContractorScheduleRequestDto, SlotAvailabilityDto, SlotBlockDto, SlotBookDto, SlotMoveDto, SlotRemoveDto, SlotRequestDto} from '../../dtos';

@Controller('schedule')
export class ScheduleController {

    constructor(private scheduleService: ScheduleService){}

    @Get('/:id')
    async getContractorsScheduleById(@Param('id') id ): Promise<BookingItemDto[]>{
        return this.scheduleService.findAContractorsCurrentSchedule(id);
    }

    @Get('/:id/month/:year/:month')
    async getContractorsScheduleByIdForMonth(@Param('id') id, @Param('year') year, @Param('month') month ): Promise<BookingItemDto[]>{
        return await this.scheduleService.findAContractorsCurrentScheduleForMonth(id, month, year);
    }

    @Get('/:id/day/:year/:day')
    async getContractorsScheduleByIdForDay(@Param('id') id, @Param('year') year, @Param('day') day ): Promise<BookingItemDto[]>{
        return await this.scheduleService.findAContractorsCurrentScheduleForDay(id, day, year);
    }

    @Post('/availability')
    async checkAvailability(@Body(new ValidationPipe()) ContractorScheduleRequestDtos: ContractorScheduleRequestDto[]  ): Promise<SlotAvailabilityDto[]>{
        const availabilities = new Array<SlotAvailabilityDto>();
        for (const ContractorScheduleRequestDto of ContractorScheduleRequestDtos){
            const result = await this.scheduleService.isAvailable(ContractorScheduleRequestDto);
            const availabilitySlot = new SlotAvailabilityDto();
            availabilitySlot.contractorId = ContractorScheduleRequestDto.itemToBook.contractorId;
            availabilitySlot.startTime = result;
            availabilities.push(availabilitySlot);
        }
        return availabilities;
    }

    @Post('/invitable')
    async whoCanBeInvited(@Body(new ValidationPipe()) ContractorScheduleRequestDtos: ContractorScheduleRequestDto[] ): Promise<Array<string>>{
        return this.scheduleService.whoCanBeInvited(ContractorScheduleRequestDtos);
    }

    @Post('/')
    async makeBooking(@Body(new ValidationPipe()) ContractorScheduleRequestDto: ContractorScheduleRequestDto ): Promise<boolean>{
        return this.scheduleService.insertBooking(ContractorScheduleRequestDto);
    }

    @Delete('/')
    async deleteBooking(@Body(new ValidationPipe()) ContractorScheduleRequestDto: ContractorScheduleRequestDto ): Promise<boolean>{
        return this.scheduleService.insertBooking(ContractorScheduleRequestDto);
    }

    @Post('/block/')
    async blockAvailability(@Body(new ValidationPipe()) slotBlock: SlotBlockDto[] ): Promise<boolean>{
        // block out availability in an array format
        return;
    }

    @Delete('/unblock/')
    async unblockAvailability(@Body(new ValidationPipe()) slotBlock: SlotBlockDto[] ): Promise<boolean>{
        // unblock out availability in an array format
        return;
    }

    @Post('/move/')
    async moveBooking(@Body(new ValidationPipe()) slotBook: SlotBookDto): Promise<boolean>{
        // remove the original booking
        // make a new booking
        return;
    }

    @Delete('/:id')
    async removeBooking(@Param(new ValidationPipe()) slotRemove: SlotRemoveDto  ): Promise<boolean>{
        // remove a booking by its ID
        return;
    }

    @Post('/createSchedules/')
    async createSchedules(@Body(new ValidationPipe()) contractors: any): Promise<void>{
        this.scheduleService.createSchedules(contractors.contractors);
        return;
    }

    // given a date a time preference and contractor and duration finds the next availabile time slot x3
        // keep the time preference
}