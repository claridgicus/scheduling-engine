import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BookingItemDto } from 'dtos';
import { TimePref } from 'dtos/enums/TimePref.dto';
import * as moment from 'moment';
import { Repository } from 'typeorm';
import * as uuid from 'uuid';
import { ContractorScheduleRequestDto } from '../dtos/contractorScheduleRequest.dto';
import { SlotBookDto } from '../dtos/slotBook.dto';
import { Schedule, timesAsInt } from '../entity/schedule.entity';
@Injectable()
export class ScheduleService {
    constructor(@InjectRepository(Schedule) private readonly scheduleRepository: Repository < Schedule > ) {}

    async createSchedules(contractors: string[]) {
        const year = moment().year();
        const month = moment().month();
        const day = moment().dayOfYear();
        // figure out todays date
        // figure out every day you have to insert
        // for every contractor id
        // insert every non existing day

        const now = moment();
        const future = moment().add(3, 'M');
        const daysBetweenThenAndNow = future.diff(now, 'days');

        for (const contractor of contractors) {
            for (let currentDay = 0; currentDay < daysBetweenThenAndNow; currentDay++) {
                const schedule = new Schedule(contractor, year, month, day + currentDay);
                const result = await this.scheduleRepository.insert(schedule);
                }
        }
    }

    async whoCanBeInvited(ContractorScheduleRequestDtos: ContractorScheduleRequestDto[]): Promise <Array<string>> {
    const contractorsToInvite = new Array<string>();
    for (const slotRequest of ContractorScheduleRequestDtos) {
        const availability: any = await this.isAvailable(slotRequest);
        if (Number.isInteger(availability)){
            contractorsToInvite.push(slotRequest.itemToBook.contractorId);
        }
    }
    return contractorsToInvite;
    }

    async acceptBooking(ContractorScheduleRequestDto: ContractorScheduleRequestDto): Promise<boolean>{
        if (await this.isAvailable(ContractorScheduleRequestDto)){

        }
        return false;
    }

    async insertBooking(ContractorScheduleRequestDto: ContractorScheduleRequestDto): Promise<boolean>{
        const contractorId = ContractorScheduleRequestDto.itemToBook.contractorId;
        const itemToBook = ContractorScheduleRequestDto.itemToBook;
        const timePref: number = ContractorScheduleRequestDto.itemToBook.timePref;
        const blockList = ContractorScheduleRequestDto.blockList;
        const scheduledDay = await this.scheduleRepository.findOne({
            where: {
                contractorId,
                day: itemToBook.day,
                year: itemToBook.year
            }
        });
        itemToBook.duration = itemToBook.duration * 2; // turn hours into half hours
        const loopableTimeSlots = scheduledDay.mapTimesToIntegerCount(scheduledDay);
        let targetDuration = 0;
        let startTime = null;
        // tslint:disable-next-line:triple-equals
        if (timePref == 1){
            for (let i = 0; i < (22 + itemToBook.duration) && 23 > i; i++){
                if (loopableTimeSlots[i] == null) {
                    targetDuration++;
                    if (startTime == null){
                        startTime = i;
                    }
                } else {
                    targetDuration = 0;
                    startTime = null;
                }
                if (targetDuration === itemToBook.duration) {
                    const result = await this.mapAndInject(ContractorScheduleRequestDto, startTime);
                    return true;
                }
            }
        } else {
            for (let i = 0; i < (42 + itemToBook.duration) && 43 > i; i++){
                if (loopableTimeSlots[i] == null) {
                    targetDuration++;
                    if (startTime == null){
                        startTime = i;
                    }
                } else {
                    targetDuration = 0;
                    startTime = null;
                }
                if (targetDuration === itemToBook.duration) {
                    const result = await this.mapAndInject(ContractorScheduleRequestDto, startTime);
                    return true;
                }
            }
        }
        return false;
    }

    async deleteBooking(ContractorScheduleRequestDto: ContractorScheduleRequestDto): Promise<boolean>{
        // to be tested - should work
        const contractorId = ContractorScheduleRequestDto.itemToBook.contractorId;
        const itemToBook = ContractorScheduleRequestDto.itemToBook;
        const scheduledDay = await this.scheduleRepository.findOne({
            where: {
                contractorId,
                day: itemToBook.day,
                year: itemToBook.year
            }
        });
        const timeSlot = scheduledDay.mapTimesToIntegerCount(scheduledDay);
        timeSlot.filter(x => x !== itemToBook.bookingId);
        const scheduleWithTimes = scheduledDay.mapsIntegerToSchedule(scheduledDay, timeSlot);
        this.scheduleRepository.update(scheduledDay.id, scheduleWithTimes);
        return true;
    }

    async mapAndInject(ContractorScheduleRequestDto: ContractorScheduleRequestDto, startTime: number): Promise<boolean>{
        const contractorId = ContractorScheduleRequestDto.itemToBook.contractorId;
        const itemToBook = ContractorScheduleRequestDto.itemToBook;
        const bookingId = ContractorScheduleRequestDto.itemToBook.bookingId;
        const scheduledDay = await this.scheduleRepository.findOne({
            where: {
                contractorId,
                day: itemToBook.day,
                year: itemToBook.year
            }
        });
        const timeSlot = scheduledDay.mapTimesToIntegerCount(scheduledDay);
        let i = startTime;
        for (let j = 0; j < (ContractorScheduleRequestDto.itemToBook.duration) && 47 > j; j++){
            timeSlot[i] =  ContractorScheduleRequestDto.itemToBook.bookingId;
            i++;
        }
        const scheduleWithoutTimes = new Schedule(contractorId, itemToBook.year, itemToBook.month, itemToBook.day);
        const scheduleWithTimes = scheduleWithoutTimes.mapsIntegerToSchedule(scheduleWithoutTimes, timeSlot);
        await this.scheduleRepository.update(scheduledDay.id, scheduleWithTimes);
        return;
    }

    async isAvailable(ContractorScheduleRequestDto: ContractorScheduleRequestDto): Promise<number | string>{
        const contractorId = ContractorScheduleRequestDto.itemToBook.contractorId;
        const itemToBook = ContractorScheduleRequestDto.itemToBook;
        const timePref = ContractorScheduleRequestDto.itemToBook.timePref;
        const blockList = ContractorScheduleRequestDto.blockList;
        const scheduledDay = await this.scheduleRepository.findOne({
            where: {
                contractorId,
                day: itemToBook.day,
                year: itemToBook.year
            }
        });
        itemToBook.duration = itemToBook.duration * 2; // turn hours into half hours
        const loopableDay = scheduledDay.mapTimesToIntegerCount(scheduledDay);
        if (timePref === 1){
            let targetDuration = 0;
            for (let i = 0; i < (23 + itemToBook.duration) && 24 > i; i++){
                if (loopableDay[i] == null) {
                    targetDuration++;
                } else {
                    targetDuration = 0;
                }
                if (targetDuration === itemToBook.duration) {
                    return i;
                }
            }
            return 'not available';
        } else {
            let targetDuration = 0;
            for (let i = 24; i < (47 + itemToBook.duration) && 48 > i; i++){
                if (loopableDay[i] == null) {
                    targetDuration++;
                } else {
                    targetDuration = 0;
                }
                if (targetDuration === itemToBook.duration) {
                    return i;
                }
            }
            return 'not available';
        }
    }

    async findAContractorsCurrentSchedule(contractorId){
        const bookingItemList = new Array<BookingItemDto>();
        const currentSchedule = await this.scheduleRepository.find({
            order: {
                day: 'ASC',
                year: 'ASC'
            },
            where: {
                contractorId,
            }
        });
        for (const day of currentSchedule){
           const bookingItems = await this.bookingDtoConstructor(day);
           bookingItems.forEach(bookingItem => {
                bookingItemList.push(bookingItem);
           });
        }
        const cleanedList = bookingItemList.filter(x => x);
        return cleanedList;
    }

    async findAContractorsCurrentScheduleForMonth(contractorId: string, month: number, year: number){
        const bookingItemList = new Array <BookingItemDto> ();
        const currentSchedule = await this.scheduleRepository.find({
            order: {
                day: 'ASC'
            },
            where: {
                contractorId,
                month,
                year,
            }
        });
        for (const day of currentSchedule) {
            const bookingItems = await this.bookingDtoConstructor(day);
            bookingItems.forEach(bookingItem => {
                bookingItemList.push(bookingItem);
            });
        }
        const cleanedList = bookingItemList.filter(x => x);
        return cleanedList;
    }

    async findAContractorsCurrentScheduleForDay(contractorId: string, day: number, year: number){
        const bookingItemList: Array<BookingItemDto> = new Array<BookingItemDto>();
        const scheduledDay = await this.scheduleRepository.findOne({
            where: {
                contractorId,
                day,
                year,
            }
        });
        const bookingItems = await this.bookingDtoConstructor(scheduledDay);
        bookingItems.forEach(bookingItem => {
            bookingItemList.push(bookingItem);
        });
        const cleanedList = bookingItemList.filter(x => x);
        return cleanedList;
    }

    async bookingDtoConstructor(day: Schedule): Promise < Array < BookingItemDto >> {
        const bookingIdsByDay = day.mapTimesToIntegerCount(day);
        const bookingItemArray = [];
        const copy = bookingIdsByDay.slice(0);
        for (let i = 0; i < bookingIdsByDay.length; i++) {
            let duration = 0;
            for (let w = 0; w < copy.length; w++) {
                if (bookingIdsByDay[i] === copy[w]) {
                    duration++;
                    delete copy[w];
                }
            }
            if (duration > 0 && bookingIdsByDay[i]) {
                const bookingItem = new BookingItemDto();
                bookingItem.bookingId = bookingIdsByDay[i];
                bookingItem.duration = duration;
                bookingItem.day = day.day;
                bookingItem.year = day.year;
                bookingItem.startTime = bookingIdsByDay.findIndex(x => x === bookingIdsByDay[i]);
                bookingItemArray.push(bookingItem);
            }
        }
        return bookingItemArray;
    }

    // async cleanSchedules(){
    //     const year = moment().year();
    //     const month = moment().month();
    //     const day = moment().dayOfYear();
    //     let contractor: string;
    //     const now = moment();
    //     const future = moment().add(3, 'M');
    //     const days = future.diff(now, 'days');
    //     for (contractor of contractors) {
    //         for (let i = 0; i < days; i++) {
    //             const schedule = new Schedule(contractor, year, month, day);
    //             const result = await this.scheduleRepository.insert(schedule);
    //         }
    //     }
    // }
}
