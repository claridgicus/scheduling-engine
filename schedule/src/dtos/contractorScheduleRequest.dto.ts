import { SlotBlockDto, SlotBookDto } from '.';
import { TimePref } from './enums/TimePref.dto';

export class ContractorScheduleRequestDto {
    itemToBook: SlotBookDto;
    blockList: SlotBlockDto;
}