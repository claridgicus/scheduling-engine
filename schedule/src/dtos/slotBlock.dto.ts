import { TimePref } from './enums/TimePref.dto';

export class SlotBlockDto {
    contractorId: string;
    day: number;
    year: number;
    time: TimePref;
    startTime: number;
    duration: number;
}