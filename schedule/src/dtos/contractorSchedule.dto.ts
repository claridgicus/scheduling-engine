import { BookingItemDto } from '.';

export class ContractorScheduleDto {
    contractorId: string;
    bookingItem: BookingItemDto[];
}