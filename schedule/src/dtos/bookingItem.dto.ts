export class BookingItemDto {
    bookingId: string;
    day: number;
    year: number;
    startTime: number;
    duration: number;
}