export class SlotAvailabilityDto {
    contractorId: string;
    startTime: number | string;
}