import { TimePref } from './enums/TimePref.dto';

export class SlotBookDto {
    contractorId: string;
    bookingId: string;
    day: number;
    month: number;
    year: number;
    startTime?: number;
    timePref?: TimePref;
    duration: number;
}
