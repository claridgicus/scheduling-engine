import { TimePref } from './enums/TimePref.dto';

export class SlotMoveDto {
    bookingId: string;
    moveToDay: number;
    moveToYear: number;
    moveToTime: number;
}