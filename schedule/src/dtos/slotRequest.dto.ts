import { TimePref } from './enums/TimePref.dto';

export class SlotRequestDto {
    contractorId: string;
    day: number;
    year: number;
    time: TimePref;
    duration: number;
}