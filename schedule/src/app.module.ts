import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from 'app.controller';
import { BaseEntity } from 'entity/base.entity';
import { Schedule } from 'entity/schedule.entity';
import { AppService } from './app.service';
import { ScheduleController } from './controllers/schedule/schedule.controller';
import { ScheduleService } from './services/schedule.service';

@Module({
  controllers: [AppController, ScheduleController],
  imports: [
    TypeOrmModule.forRoot(),
    TypeOrmModule.forFeature([Schedule])
  ],
  providers: [AppService, ScheduleService],
  })
export class AppModule {
}
