import * as nodemailer from 'nodemailer';
const transporter = nodemailer.createTransport({
    auth: {
        pass: '',
        user: ''
    },
    host: 'email-smtp.us-west-2.amazonaws.com',
    port: 25,
});

module.exports = function(params: any) {
    this.from = '';

    this.send = function() {
        const options = {
            from: this.from,
            html: params.html,
            subject: params.subject,
            text: params.message,
            to: params.to,
        };

        transporter.sendMail(options, function(err, suc) {
            err ? params.errorCallback(err) : params.successCallback(suc);
        });
    };
};