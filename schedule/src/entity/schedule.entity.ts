import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BaseEntity } from './base.entity';

@Entity()
export class Schedule extends BaseEntity {

    constructor(contractorId: string, year: number, month: number, day: number){
        super();
        this.contractorId = contractorId;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    contractorId: string;

    @Column()
    year: number;

    @Column()
    month: number;

    @Column()
    day: number;

    @Column({nullable: true})
    t0000: string;
        @Column({nullable: true})
    t0030: string;
        @Column({nullable: true})
    t0100: string;
        @Column({nullable: true})
    t0130: string;
        @Column({nullable: true})
    t0200: string;
        @Column({nullable: true})
    t0230: string;
        @Column({nullable: true})
    t0300: string;
        @Column({nullable: true})
    t0330: string;
        @Column({nullable: true})
    t0400: string;
        @Column({nullable: true})
    t0430: string;
        @Column({nullable: true})
    t0500: string;
        @Column({nullable: true})
    t0530: string;
        @Column({nullable: true})
    t0600: string;
        @Column({nullable: true})
    t0630: string;
        @Column({nullable: true})
    t0700: string;
        @Column({nullable: true})
    t0730: string;
        @Column({nullable: true})
    t0800: string;
        @Column({nullable: true})
    t0830: string;
        @Column({nullable: true})
    t0900: string;
        @Column({nullable: true})
    t0930: string;
        @Column({nullable: true})
    t1000: string;
        @Column({nullable: true})
    t1030: string;
        @Column({nullable: true})
    t1100: string;
        @Column({nullable: true})
    t1130: string;
        @Column({nullable: true})
    t1200: string;
        @Column({nullable: true})
    t1230: string;
        @Column({nullable: true})
    t1300: string;
        @Column({nullable: true})
    t1330: string;
        @Column({nullable: true})
    t1400: string;
        @Column({nullable: true})
    t1430: string;
        @Column({nullable: true})
    t1500: string;
        @Column({nullable: true})
    t1530: string;
        @Column({nullable: true})
    t1600: string;
        @Column({nullable: true})
    t1630: string;
        @Column({nullable: true})
    t1700: string;
        @Column({nullable: true})
    t1730: string;
        @Column({nullable: true})
    t1800: string;
        @Column({nullable: true})
    t1830: string;
        @Column({nullable: true})
    t1900: string;
        @Column({nullable: true})
    t1930: string;
        @Column({nullable: true})
    t2000: string;
        @Column({nullable: true})
    t2030: string;
        @Column({nullable: true})
    t2100: string;
        @Column({nullable: true})
    t2130: string;
        @Column({nullable: true})
    t2200: string;
        @Column({nullable: true})
    t2230: string;
        @Column({nullable: true})
    t2300: string;
        @Column({nullable: true})
    t2330: string;

    timeAsStr(time: number){
        const key = Object.keys(timesAsInt).find(k => timesAsInt[k] === time);
        return key;
    }

    mapTimesToIntegerCount(row: Schedule){
        const timeSlot = new Array<string>();

        timeSlot[0] = row.t0000;
        timeSlot[1] = row.t0030;
        timeSlot[2] = row.t0100;
        timeSlot[3] = row.t0130;
        timeSlot[4] = row.t0200;
        timeSlot[5] = row.t0230;
        timeSlot[6] = row.t0300;
        timeSlot[7] = row.t0330;
        timeSlot[8] = row.t0400;
        timeSlot[9] = row.t0430;
        timeSlot[10] = row.t0500;
        timeSlot[11] = row.t0530;
        timeSlot[12] = row.t0600;
        timeSlot[13] = row.t0630;
        timeSlot[14] = row.t0700;
        timeSlot[15] = row.t0730;
        timeSlot[16] = row.t0800;
        timeSlot[17] = row.t0830;
        timeSlot[18] = row.t0900;
        timeSlot[19] = row.t0930;
        timeSlot[20] = row.t1000;
        timeSlot[21] = row.t1030;
        timeSlot[22] = row.t1100;
        timeSlot[23] = row.t1130;
        timeSlot[24] = row.t1200;
        timeSlot[25] = row.t1230;
        timeSlot[26] = row.t1300;
        timeSlot[27] = row.t1330;
        timeSlot[28] = row.t1400;
        timeSlot[29] = row.t1430;
        timeSlot[30] = row.t1500;
        timeSlot[31] = row.t1530;
        timeSlot[32] = row.t1600;
        timeSlot[33] = row.t1630;
        timeSlot[34] = row.t1700;
        timeSlot[35] = row.t1730;
        timeSlot[36] = row.t1800;
        timeSlot[37] = row.t1830;
        timeSlot[38] = row.t1900;
        timeSlot[39] = row.t1930;
        timeSlot[40] = row.t2000;
        timeSlot[41] = row.t2030;
        timeSlot[42] = row.t2100;
        timeSlot[43] = row.t2130;
        timeSlot[44] = row.t2200;
        timeSlot[45] = row.t2230;
        timeSlot[46] = row.t2300;
        timeSlot[47] = row.t2330;
        return timeSlot;
    }

    mapsIntegerToSchedule(schedule: Schedule, timeSlot: Array<string>){

        const row  = new Schedule(schedule.contractorId, schedule.year, schedule.month, schedule.day);

        row.t0000 = timeSlot[0];
        row.t0030 = timeSlot[1];
        row.t0100 = timeSlot[2];
        row.t0130 = timeSlot[3];
        row.t0200 = timeSlot[4];
        row.t0230 = timeSlot[5];
        row.t0300 = timeSlot[6];
        row.t0330 = timeSlot[7];
        row.t0400 = timeSlot[8];
        row.t0430 = timeSlot[9];
        row.t0500 = timeSlot[10];
        row.t0530 = timeSlot[11];
        row.t0600 = timeSlot[12];
        row.t0630 = timeSlot[13];
        row.t0700 = timeSlot[14];
        row.t0730 = timeSlot[15];
        row.t0800 = timeSlot[16];
        row.t0830 = timeSlot[17];
        row.t0900 = timeSlot[18];
        row.t0930 = timeSlot[19];
        row.t1000 = timeSlot[20];
        row.t1030 = timeSlot[21];
        row.t1100 = timeSlot[22];
        row.t1130 = timeSlot[23];
        row.t1200 = timeSlot[24];
        row.t1230 = timeSlot[25];
        row.t1300 = timeSlot[26];
        row.t1330 = timeSlot[27];
        row.t1400 = timeSlot[28];
        row.t1430 = timeSlot[29];
        row.t1500 = timeSlot[30];
        row.t1530 = timeSlot[31];
        row.t1600 = timeSlot[32];
        row.t1630 = timeSlot[33];
        row.t1700 = timeSlot[34];
        row.t1730 = timeSlot[35];
        row.t1800 = timeSlot[36];
        row.t1830 = timeSlot[37];
        row.t1900 = timeSlot[38];
        row.t1930 = timeSlot[39];
        row.t2000 = timeSlot[40];
        row.t2030 = timeSlot[41];
        row.t2100 = timeSlot[42];
        row.t2130 = timeSlot[43];
        row.t2200 = timeSlot[44];
        row.t2230 = timeSlot[45];
        row.t2300 = timeSlot[46];
        row.t2330 = timeSlot[47];
        return row;
    }
}

export enum timesAsInt {
    t0000 = 0,
    t0030 = 1,
    t0100 = 2,
    t0130 = 3,
    t0200 = 4,
    t0230 = 5,
    t0300 = 6,
    t0330 = 7,
    t0400 = 8,
    t0430 = 9,
    t0500 = 10,
    t0530 = 11,
    t0600 = 12,
    t0630 = 13,
    t0700 = 14,
    t0730 = 15,
    t0800 = 16,
    t0830 = 17,
    t0900 = 18,
    t0930 = 29,
    t1000 = 20,
    t1030 = 21,
    t1100 = 22,
    t1130 = 23,
    t1200 = 24,
    t1230 = 25,
    t1300 = 26,
    t1330 = 27,
    t1400 = 28,
    t1430 = 39,
    t1500 = 30,
    t1530 = 31,
    t1600 = 32,
    t1630 = 33,
    t1700 = 34,
    t1730 = 35,
    t1800 = 36,
    t1830 = 37,
    t1900 = 38,
    t1930 = 49,
    t2000 = 40,
    t2030 = 41,
    t2100 = 42,
    t2130 = 43,
    t2200 = 44,
    t2230 = 45,
    t2300 = 46,
    t2330 = 47,
}



