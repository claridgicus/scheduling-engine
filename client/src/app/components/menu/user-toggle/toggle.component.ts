import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthToken, MagicLogin } from '../../../classes';
import { AuthenticationService } from '../../../services';

@Component({
  selector: 'user-toggle',
  styleUrls: ['./toggle.component.scss'],
  templateUrl: './toggle.component.html'
})

export class UserToggleComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}