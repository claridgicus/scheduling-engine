import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthToken, MagicLogin } from '../../../classes';
import { AuthenticationService } from '../../../services';

@Component({
  selector: 'menu-primary',
  styleUrls: ['./primary.component.scss'],
  templateUrl: './primary.component.html'
})

export class PrimaryMenuComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}