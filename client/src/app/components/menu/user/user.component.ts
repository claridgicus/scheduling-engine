import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../../../classes';
import { UserService } from '../../../services';


@Component({
  selector: 'menu-user',
  styleUrls: ['./user.component.scss'],
  templateUrl: './user.component.html'
})

export class UserMenuComponent implements OnInit {
  user: any;

  constructor(private userService: UserService) {
    this.user = new User();
    this.userService.getCurrentUser().subscribe(
      (user: User) => { this.user = user; }
    );
  }

  ngOnInit() {
  }

}