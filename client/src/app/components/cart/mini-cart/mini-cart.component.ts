import { Component, OnInit } from '@angular/core';
import { CartService } from './../../../services/index';
import { Cart, CartItem, RepetitionEnum } from '../../../classes/index';

@Component({
  selector: 'app-mini-cart',
  templateUrl: './mini-cart.component.html',
  styleUrls: ['./mini-cart.component.scss']
})
export class MiniCartComponent implements OnInit {
  cart: Cart;

  constructor(private cartService: CartService) {
    this.cart = new Cart();
   }

  ngOnInit() {
      this.cart = this.cartService.getCurrentCart();
  }

  removeFromCart(cartItem) {
    console.log(cartItem.id)
    this.cart = this.cartService.removeFromCart(cartItem.id);
  }

  printStatus(rep: number){
    const key = Object.keys(RepetitionEnum).find(k => RepetitionEnum[k] === rep);
    return key;
  }

  placeBooking(){
    console.log('booking');
    this.cart = this.cartService.placeBooking();
  }
}
