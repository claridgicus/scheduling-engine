import {APP_BASE_HREF} from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatInputModule } from '@angular/material';
import { AuthLayoutComponent, ExpiredComponent, FullscreenLayoutComponent, LoginComponent, ResetComponent, SetComponent, SignupComponent, VerifyComponent } from '../..';
import { AppRoutingModule } from '../../../app-routing.module';
import { EmptyLayoutComponent } from './empty-layout.component';

describe('EmptyLayoutComponent', () => {
  let component: EmptyLayoutComponent;
  let fixture: ComponentFixture<EmptyLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmptyLayoutComponent, AuthLayoutComponent, SetComponent, ResetComponent, VerifyComponent, ExpiredComponent, FullscreenLayoutComponent, LoginComponent, SignupComponent, ],
      imports: [ BrowserAnimationsModule, RouterModule, AppRoutingModule, FormsModule,  MatButtonModule, MatCheckboxModule, MatInputModule],
      providers: [ {provide: APP_BASE_HREF, useValue: '/'}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
