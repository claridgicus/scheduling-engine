import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { CartService, UserService } from '../../../services/index';
import { User } from '../../../classes';

@Component({
  selector: 'app-window-layout',
  templateUrl: './window-layout.component.html',
  styleUrls: ['./window-layout.component.scss']
})
export class WindowLayoutComponent implements OnInit {
  currentYear: string;
  termsandconditions: boolean;
  user: User;

  constructor(private cartService: CartService, private userService: UserService) {
    this.currentYear = new Date().getFullYear().toString();

    this.user = new User();
    this.userService.getCurrentUser().subscribe(
      (user: User) => { 
        console.log(user);
        this.user = user; }
    );
  }

  ngOnInit() {
  }
}
