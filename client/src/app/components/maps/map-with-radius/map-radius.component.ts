import { Appearance, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { Component, Input, OnInit, Output } from '@angular/core';
import PlaceResult = google.maps.places.PlaceResult;
import { UserService } from '../../../services';
import { LocationDto, LocationPinDto, User } from './../../../classes/index';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'map-with-radius',
  templateUrl: './map-radius.component.html',
  styleUrls: ['./map-radius.component.scss']
})

export class MapWithRadiusComponent implements OnInit {
  @Input() rangeInput: boolean;
  @Input() showInput: boolean;
  @Input() autoLocate: boolean;
  @Input() pinType: number;
  @Output() readonly saved = new EventEmitter<boolean>();

  pin: boolean;
  range: number;
  location: LocationDto;
  locationPin: LocationPinDto;
  locationSet: boolean;
  zoom: number;
  selectedAddress: PlaceResult;
  position: Position;
  protected map: any;



  constructor(private userService: UserService) {
    this.locationSet = false;
    this.zoom = 11;
    this.range = 10000;
    this.locationPin = new LocationPinDto();
    this.locationPin.type ? this.locationPin.type = this.pinType : this.locationPin.type = 1;
    this.locationPin.lat = 37.8136;
    this.locationPin.lng = 144.9631;
    const pin = false;
    this.userService.getCurrentUserPin(this.locationPin.type).subscribe(
      (pinResult: LocationPinDto) => {
        console.log(pinResult);
        if (pinResult) {
          this.locationPin = pinResult;
          this.pin = true;
          this.locationSet = true;
        } else {
        this.setCurrentPosition();
        }
      },
      (err) => {},
      () => { }
    );
  }

  ngOnInit() {
    if (this.autoLocate){
      this.setCurrentPosition();
      this.zoom = 11;
    }
  }

  // ngAfterViewInit() {
  //   if (this.autoLocate){
  //     this.setCurrentPosition();
  //     this.zoom = 11;
  //   }
  // }

  changeInput(event){
    this.range = event;
    this.userService.setRange(this.range).subscribe(
      (data: boolean) => {
        console.log('updated');
      }
    )
  }

  protected mapReady(map) {
    this.map = map;
    if (this.map){
      this.locationPin.lat = Number(this.locationPin.lat);
      this.locationPin.lng = Number(this.locationPin.lng);
    }
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.locationPin.lat = position.coords.latitude;
        this.locationPin.lng = position.coords.longitude;
        this.pin = true;
      });
    }
  }

  // formatLabel(value: number | null) {
  //   if (!value) {
  //     return 10000;
  //   }
  //   if (value >= 1000) {
  //     return Math.round(value / 1000) + 'km';
  //   }
  //   return value;
  // }

  // onAddressSelected(result: PlaceResult) {
  //   console.log('onAddressSelected: ', result);
  // }

  onLocationSelected(selectedAddress: any) {
    console.log(selectedAddress);
    this.locationPin.lat = selectedAddress.latitude;
    this.locationPin.lng = selectedAddress.longitude;
    this.selectedAddress = selectedAddress;
    this.pin = true;
  }

  saveLocation(){
    this.saved.emit(true);
    this.userService.upsertCurrentUserPin(this.locationPin).subscribe(
      (user: User) => { console.log(user); }
    );
  }

}
