import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../../../services/index';

@Component({
  selector: 'booking-service-selection',
  templateUrl: './service-selection.component.html',
  styleUrls: ['./service-selection.component.scss']
})

export class ServiceSelectionComponent implements OnInit {
services: any;
selection: string;

  constructor(private serviceService: ServiceService) {
    this.selection = null;
    this.serviceService.getServices().subscribe(
      (services: any) => { this.services = services; }
    );
   }

  ngOnInit() {
  }

}
