import { Component, OnInit } from '@angular/core';
import { BookingService } from '../../../services';

@Component({
  selector: 'booking-list',
  templateUrl: './list-booking.component.html',
  styleUrls: ['./list-booking.component.scss']
})

export class ListBookingComponent implements OnInit {
  bookings: any;

  constructor(private bookingService: BookingService) {
    this.bookings = [];
    this.bookingService.getBookingItemComponents().subscribe(
      (bookings) => {
        console.log(bookings);
        this.bookings = bookings;
      }
    );
   }

  ngOnInit() {
  }

}
