import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
accepted: boolean;
postcode: number;

  constructor() {
    this.accepted = null;
  }

  ngOnInit() {
  }

  submit(){
    // check the list against input
    // return true or false
    // set accepted true or false
    this.accepted = true;
  }
  // event(){
  //   this.saved = true;

  //   console.log(this.saved);
  // }
}