import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../classes/user';
import { UserService } from '../../../services';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})

export class VerifyComponent implements OnInit {
  error: string;
  email: string;
  success: boolean;

  constructor(private activatedRoute: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      (data) => {
        this.success = true;
        this.email = data.email;
      }
    );
  }

  resend(){
    this.userService.resendActivationEmail(this.email).subscribe(
      (data) => {
        this.success = true;
      }
    );
  }
}
