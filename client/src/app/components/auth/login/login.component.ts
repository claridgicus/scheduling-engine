import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthToken, MagicLogin } from '../../../classes';
import { AuthenticationService } from '../../../services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  email: string;
  password: string;
  user: string;
  token: string;
  auth: AuthToken;
  magicLogin: MagicLogin;
  attributionResult: number;

  constructor(private authService: AuthenticationService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.authService.logout();
    this.route.queryParams.subscribe(
      (params: Params) => {
        if (params.token && params.user && params.redirect){
          this.magicLogin = new MagicLogin(params.token, params.user, params.redirect);
          this.logInWithMagic(this.magicLogin);
        }
      }
    );
  }

  // tested true
  login() {
    this.authService.login(this.email, this.password).subscribe(
      (token: AuthToken) => {
        this.authService.setToken(token.token);
        this.router.navigate(['/dashboard']);
      }
    );
  }

  // tested true
  logInWithMagic(magicLogin: MagicLogin) {
    const auth = magicLogin.reduceToAuthToken(magicLogin);
    this.authService.magicLogin(auth).subscribe(
      (token: any) => {
        this.authService.setToken(token.token);
        this.router.navigate([magicLogin.redirect]);
      }
    );
  }

}
