import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../../classes/user';
import { AuthenticationService } from '../../../services';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  user: User;
  error: string;
  success: string;
  isContractor: boolean;

  constructor(private authService: AuthenticationService, private router: Router, private route: ActivatedRoute) {
    this.user = new User();
    route.snapshot.data[0].isContractor ? this.isContractor = route.snapshot.data[0].isContractor : this.isContractor =  null;

  }

  ngOnInit() {
  }

  createAccount() {
    const email = this.user.email;
    if (this.isContractor){
      this.authService.createContractor(this.user).subscribe(
        accountCreated => {
          this.router.navigate(['/verify'], { queryParams: {email}});
        });
    } else {
    this.authService.createUser(this.user).subscribe(
      accountCreated => {
        this.router.navigate(['/verify'], { queryParams: {email}});
      });
    }
  }
}
