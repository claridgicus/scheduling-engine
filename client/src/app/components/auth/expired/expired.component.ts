import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../../../classes/user';
import { AuthenticationService } from '../../../services';

@Component({
  selector: 'app-expired',
  templateUrl: './expired.component.html',
  styleUrls: ['./expired.component.scss']
})
export class ExpiredComponent implements OnInit, OnDestroy {
  user: User;
  error: string;
  success: string;
  password: string;
  updatePasswordSubscription: Subscription;

  constructor(private authService: AuthenticationService, private router: Router ) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.updatePasswordSubscription.unsubscribe();
  }

  updatePassword() {
    this.updatePasswordSubscription = this.authService.updatePassword(this.user, this.password).subscribe(
      passwordChanged => {
        this.router.navigate(['/overview']);
       });
  }
}
