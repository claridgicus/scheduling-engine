import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../../classes/user';
import { AuthenticationService } from '../../../services';

@Component({
  selector: 'app-set',
  styleUrls: ['./set.component.scss'],
  templateUrl: './set.component.html'
})
export class SetComponent implements OnInit {
  user: User;
  error: string;
  success: string;
  password: string;
  passwordConfirm: string;

  constructor(private authService: AuthenticationService, private router: Router ) {

  }

  ngOnInit() {

  }

  updatePassword() {
    if (this.password === this.passwordConfirm){
      this.authService.updatePassword(this.user, this.password).subscribe(
        passwordUpdated => {
          this.router.navigate(['/dashboard']);
        });
    }
  }
}
