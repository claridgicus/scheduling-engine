import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../../../classes';
import { UserService } from '../../../services';
@Component({
  selector: 'dashboard-client',
  styleUrls: ['./client.component.scss'],
  templateUrl: './client.component.html'
})
export class ClientDashboardComponent implements OnInit {
  currentYear: string;
  user: User;
  selection: string;

  constructor(private route: ActivatedRoute, private userService: UserService) {
    this.currentYear = new Date().getFullYear().toString();
    this.user = new User();
    this.userService.getCurrentUser().subscribe(
      (user: User) => { this.user = user; }
    );
    this.route.params.subscribe(
      (routeParams) => { routeParams.id ? this.selection = routeParams.id : this.selection = 'make'; }
    );
  }

  ngOnInit() {

  }
}
