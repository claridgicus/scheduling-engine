import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { ServiceService, CartService } from './../../../services/index';
import { ServiceDto, Cart, CartItem, SubCartItem } from '../../../classes';

@Component({
  selector: 'app-single-service',
  templateUrl: './single-service.component.html',
  styleUrls: ['./single-service.component.scss']
})
export class SingleServiceComponent implements OnInit {
  cartItem: CartItem;
  service: ServiceDto;
  routeParams: any;
  cart: Cart;
  tomorrow: Date;

  constructor(private cartService: CartService, private route: ActivatedRoute, private serviceService: ServiceService) { 
    this.service = new ServiceDto();
    this.newItemInit();
    this.tomorrow = moment(new Date()).add(1, 'days').toDate();
    this.route.params.subscribe(
      (routeParams) => { this.routeParams = routeParams; }
    );
    this.serviceService.getServiceById(this.routeParams.id).subscribe(
      (service: ServiceDto) => {
        this.service = service;
        this.cartItem.service = this.service;
      }
    );

  }

  newItemInit(){
    // Set some sane defaults for when the page loads / form is refeshed on submit / cancel
    this.cartItem = new CartItem();
    this.cartItem.cartItemComponents = new Array<SubCartItem>();
    const cartItemComponent = new SubCartItem(null, null);
    this.cartItem.cartItemComponents.push(cartItemComponent);
    this.cartItem.cartItemComponents[0].date = new Date();
    this.cartItem.duration = 1;
    this.cartItem.repetitions = 1;
    this.cartItem.repetition = 0;
    this.cartItem.timePref = 1;
    this.cartItem.startDate = this.tomorrow;
    this.cartItem.service = this.service || new ServiceDto();
  }


  ngOnInit() {

  }

  costType() {
    for (let i = 0; i < this.cartItem.cartItemComponents.length; i++ ){
      const currentItem = this.cartItem.cartItemComponents[i];
      const date = moment(currentItem.date);
      const day = date.day();
      if (day == 6 || day == 0){
        currentItem.cost = this.service.weekendCost;
      } else {
        if (this.cartItem.timePref == 1){
          currentItem.cost = this.service.morningCost;
        } else if (this.cartItem.timePref == 2){
          currentItem.cost = this.service.afternoonCost;
        }
      }
    }

  }

  calculateTotal(){
    this.cartItem.total = 0;
    for (let i = 0; i < this.cartItem.cartItemComponents.length; i++ ){
      this.cartItem.total = this.cartItem.total + (this.cartItem.cartItemComponents[i].cost * this.cartItem.duration);
    }
  }

  setDate(event){
    this.cartItem.startDate = event;
    this.setRepetitions(this.cartItem.repetitions);
  }

  setTimePref(event){
    this.cartItem.timePref = event;
    this.setRepetitions(this.cartItem.repetitions);
  }

  setRepetition(event){
    this.cartItem.repetition = event;
    this.setRepetitions(this.cartItem.repetitions);
  }

  setRepetitions(event){
    this.cartItem.repetitions = event;
    if (this.cartItem.repetition === 0){
      this.cartItem.repetitions = 1;
    }
    this.cartItem.cartItemComponents = new Array<SubCartItem>();
    for (let i = 0; i < this.cartItem.repetitions; i++){
      let date = this.cartItem.startDate;
      if (this.cartItem.repetition == 1){
        date = moment(this.cartItem.startDate).add(i, 'days').toDate();
      } else if (this.cartItem.repetition == 2){
        date = moment(this.cartItem.startDate).add(i, 'weeks').toDate();
      } else if (this.cartItem.repetition == 3){
        date = moment(this.cartItem.startDate).add((2 * i), 'weeks').toDate();
      } else if (this.cartItem.repetition == 4){
        date = moment(this.cartItem.startDate).add(i, 'months').toDate();
      }


      const cartItemComponent = new SubCartItem(null, date);
      this.cartItem.cartItemComponents.push(cartItemComponent);
    }
    this.costType();
    this.calculateTotal();
  }

  addToCart() {
    this.costType();
    this.calculateTotal();
    this.cartService.addToCart(this.cartItem);
    this.newItemInit();
  }

}
