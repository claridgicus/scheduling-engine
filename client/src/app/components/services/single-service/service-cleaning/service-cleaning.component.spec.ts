import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceCleaningComponent } from './service-cleaning.component';

describe('ServiceCleaningComponent', () => {
  let component: ServiceCleaningComponent;
  let fixture: ComponentFixture<ServiceCleaningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceCleaningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceCleaningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
