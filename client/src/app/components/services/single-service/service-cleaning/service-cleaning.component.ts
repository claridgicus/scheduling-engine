import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { ServiceDto } from '../../../../classes/index';

@Component({
  selector: 'service-service-cleaning',
  templateUrl: './service-cleaning.component.html',
  styleUrls: ['./service-cleaning.component.scss']
})
export class ServiceCleaningComponent implements OnInit {
  @Input() service: ServiceDto;

  constructor() {
    // this.service = new ServiceDto();
   }

  ngOnInit() {
  }

}
