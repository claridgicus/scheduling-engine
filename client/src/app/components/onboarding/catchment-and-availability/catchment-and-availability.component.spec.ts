import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatchmentAndAvailabilityComponent } from './catchment-and-availability.component';

describe('CatchmentAndAvailabilityComponent', () => {
  let component: CatchmentAndAvailabilityComponent;
  let fixture: ComponentFixture<CatchmentAndAvailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatchmentAndAvailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatchmentAndAvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
