import { Component, OnInit } from '@angular/core';
import { User } from '../../classes';
import { UserService } from '../../services';

@Component({
  selector: 'onboarding-parent',
  templateUrl: './onboarding-parent.component.html',
  styleUrls: ['./onboarding-parent.component.scss']
})

export class OnboardingParentComponent implements OnInit {
user: User;

  constructor(private userService: UserService) {
    this.user = new User();
    this.user.contractor = false;
    this.userService.getCurrentUser().subscribe(
      (user: User) => { this.user = user; }
    )
  }

  ngOnInit() {
  }


}
