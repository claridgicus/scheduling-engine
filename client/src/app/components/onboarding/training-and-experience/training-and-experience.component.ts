import { Component, OnInit } from '@angular/core';
import { User } from '../../../classes';
import { ServiceService, UserService } from '../../../services';

@Component({
  selector: 'app-training-and-experience',
  templateUrl: './training-and-experience.component.html',
  styleUrls: ['./training-and-experience.component.scss']
})
export class TrainingAndExperienceComponent implements OnInit {
  services: any;
  selection: string;
  serviceSet: boolean;
  user: User;

  constructor(private serviceService: ServiceService, private userService: UserService) {
    this.selection = null;
    this.serviceSet = true;
    this.userService.getCurrentUser().subscribe(
      (user: User) => {
        this.user = user;
        console.log(user);
        this.user.service ? this.selection = user.service : this.selection = null;
        this.user.service ? this.serviceSet = true : this.serviceSet = false;
      }
    );
    this.serviceService.getServices().subscribe(
      (services: any) => {
        this.services = services;
      }
    );
  }

  ngOnInit() {}

  saveService() {
    if (this.selection){
      this.serviceService.setUserService(this.selection).subscribe(
      (success: boolean) => {
        this.user.service = this.selection;
        this.serviceSet = true;
        }
      );
    }
  }

}