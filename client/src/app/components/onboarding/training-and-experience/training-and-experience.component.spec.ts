import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingAndExperienceComponent } from './training-and-experience.component';

describe('TrainingAndExperienceComponent', () => {
  let component: TrainingAndExperienceComponent;
  let fixture: ComponentFixture<TrainingAndExperienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingAndExperienceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingAndExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
