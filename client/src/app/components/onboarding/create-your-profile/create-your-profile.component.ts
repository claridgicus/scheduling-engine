import { Component, OnInit } from '@angular/core';
import { User, LocationDto } from '../../../classes';
import { UserService } from '../../../services';

@Component({
  selector: 'app-create-your-profile',
  templateUrl: './create-your-profile.component.html',
  styleUrls: ['./create-your-profile.component.scss']
})

export class CreateYourProfileComponent implements OnInit {
  user: User;
  submissionComplete: boolean;

  constructor(private userService: UserService) {
    this.submissionComplete = false;
    this.user = new User();
    this.user.location = new LocationDto();
    this.userService.getCurrentUser().subscribe(
      (user: User) => {
        user ? this.user = user : this.user = new User();
        user.location ? this.user.location = user.location : this.user.location = new LocationDto(); }
    );
  }

  ngOnInit() {

  }

  submit(){
    this.userService.updateCurrentUser(this.user).subscribe(
      (data: boolean) => {
        console.log(this.user);
        this.submissionComplete = true;
      }
    );
  }

  next(){
    // this.router.navigate
  }

}