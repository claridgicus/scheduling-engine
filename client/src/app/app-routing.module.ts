// tslint:disable:object-literal-sort-keys
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// tslint:disable-next-line:max-line-length
import {
  CatchmentAndAvailabilityComponent,
  ClientDashboardComponent,
  ComplianceComponent,
  CreateYourProfileComponent,
  EmptyLayoutComponent,
  ExpiredComponent,
  FullscreenLayoutComponent,
  LocationComponent,
  LoginComponent,
  MapWithRadiusComponent,
  OnboardingParentComponent,
  ResetComponent,
  SetComponent,
  SettingsComponent,
  SignupComponent,
  SingleServiceComponent,
  TrainingAndExperienceComponent,
  VerifyComponent,
  WindowLayoutComponent
} from './components';
import { AuthGuard, OnboardGuard } from './guards';

const routes: Routes = [
  {
  path: '', component: FullscreenLayoutComponent,
    children: [
      // { path: '', component: LoginComponent },
      { path: 'login', component: LoginComponent },
      { path: 'signup', component: SignupComponent, data: [{isContractor: false}]  },
      { path: 'set', component: SetComponent, canActivate: [AuthGuard] },
      { path: 'verify', component: VerifyComponent },
      { path: 'expired', component: ExpiredComponent },
      { path: 'reset', component: ResetComponent },
    ]
  },
  {
    path: 'contractor', component: FullscreenLayoutComponent,
    children: [
      { path: 'signup', component: SignupComponent, data: [{isContractor: true}] },
    ]
  },
  {
    path: '', component: WindowLayoutComponent, canActivate: [AuthGuard, OnboardGuard],
    children: [
      { path: 'dashboard', component: ClientDashboardComponent },
      { path: 'dashboard/:id', component: ClientDashboardComponent },
      { path: 'service', component: EmptyLayoutComponent,
        children: [
          { path: ':id', component: SingleServiceComponent },
        ]
      },
    ]
  },
  {
    path: '', component: WindowLayoutComponent, canActivate: [AuthGuard],
    children: [
      { path: 'onboarding', component: OnboardingParentComponent,
      children: [
        { path: 'create-your-profile', component: CreateYourProfileComponent },
        { path: 'training-and-experience', component: TrainingAndExperienceComponent },
        { path: 'catchment-and-availability', component: CatchmentAndAvailabilityComponent },
        { path: 'compliance', component: ComplianceComponent },
        { path: 'settings', component: SettingsComponent },
      ] },
    ]
  }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})

export class AppRoutingModule { }
