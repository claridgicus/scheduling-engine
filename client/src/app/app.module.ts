import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { registerLocaleData } from '@angular/common';
import localeEn from '@angular/common/locales/en';
import localeEnExtra from '@angular/common/locales/extra/en';
registerLocaleData(localeEn,localeEnExtra);
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatInputModule, MatNativeDateModule, MatSliderModule, MatSelectModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  CatchmentAndAvailabilityComponent,
  ClientDashboardComponent,
  ComplianceComponent,
  CreateYourProfileComponent,
  EmptyLayoutComponent,
  ExpiredComponent,
  FullscreenLayoutComponent,
  ListBookingComponent,
  ListNotificationComponent,
  LocationComponent,
  LoginComponent,
  MapWithRadiusComponent,
  OnboardingParentComponent,
  PrimaryMenuComponent,
  ResetComponent,
  ServiceCleaningComponent,
  ServiceSelectionComponent,
  SetComponent,
  SettingsComponent,
  SignupComponent,
  SingleServiceComponent,
  TrainingAndExperienceComponent,
  UserMenuComponent,
  UserToggleComponent,
  VerifyComponent,
  WindowLayoutComponent,
  MiniCartComponent
 } from './components';
import { AuthGuard, OnboardGuard } from './guards';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import {
  AuthenticationService,
  CartService,
  ServiceService,
  UserService,
  BookingService
} from './services';

@NgModule({
    bootstrap: [
      AppComponent
    ],
    declarations: [
      ExpiredComponent,
      EmptyLayoutComponent,
      FullscreenLayoutComponent,
      WindowLayoutComponent,
      LoginComponent,
      SetComponent,
      AppComponent,
      ResetComponent,
      SignupComponent,
      VerifyComponent,
      PrimaryMenuComponent,
      UserToggleComponent,
      ClientDashboardComponent,
      UserMenuComponent,
      ServiceSelectionComponent,
      ListBookingComponent,
      SingleServiceComponent,
      ListNotificationComponent,
      MapWithRadiusComponent,
      OnboardingParentComponent,
      CreateYourProfileComponent,
      TrainingAndExperienceComponent,
      CatchmentAndAvailabilityComponent,
      ComplianceComponent,
      SettingsComponent,
      LocationComponent,
      ServiceCleaningComponent,
      MiniCartComponent,
    ],
    imports: [
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      AppRoutingModule,
      RouterModule,
      HttpModule,
      AgmCoreModule.forRoot({
        apiKey: 'AIzaSyCooVE49fOsyww1NDHkYbxpnV2gODj8r1E',
        libraries: ['places']
      }),
      MatGoogleMapsAutocompleteModule.forRoot(),
      BrowserAnimationsModule,
      HttpClientModule,
      NgHttpLoaderModule,
      BrowserAnimationsModule,
      MatButtonModule,
      MatCheckboxModule,
      MatSelectModule,
      MatInputModule,
      MatSliderModule,
      MatDatepickerModule,
      MatNativeDateModule
    ],
    providers: [{
      multi: true,
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      },
      AuthGuard,
      OnboardGuard,
      // UserTypeGuard,
      AuthenticationService,
      UserService,
      CartService,
      BookingService,
      ServiceService
    ],
    schemas: [NO_ERRORS_SCHEMA],
  })
  export class AppModule {}
