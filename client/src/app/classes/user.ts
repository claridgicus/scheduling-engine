import { LocationDto, LocationPinDto } from './index';
export class User {
    id: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    title?: string;
    phone?: string;
    location?: LocationDto;
    locationPin?: LocationPinDto[];
    contractor: boolean;
    service: any;
    public fullName() {
        return this.firstName + ' ' + this.lastName;
    }
}
