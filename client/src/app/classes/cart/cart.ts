import { ServiceDto } from '../index';

export class Cart {
    items: CartItem[];
    total: number;
}

export class CartItem {
    id?: string;
    service: ServiceDto;
    duration: number;
    cartItemComponents: SubCartItem[];
    repetition: number;
    repetitions: RepetitionEnum;
    timePref: number;
    total: number;
    startDate: Date;
}

export class SubCartItem {
    date: Date;
    cost: number;

    constructor(cost: number, date?: Date){
        this.cost = cost;
        this.date = date || new Date();
    }
}

export enum RepetitionEnum {
    Once = 0,
    Daily = 1,
    Weekly = 2,
    Fortnightly = 3,
    Monthly = 4
}