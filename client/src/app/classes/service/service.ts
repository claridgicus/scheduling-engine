export class ServiceDto {
    id?: string;
    serviceName: string;
    description: string;
    excerpt: string;
    imageUrl: string;
    subservice: SubServiceDto[];
    morningCost: number;
    afternoonCost: number;
    publicCost: number;
    weekendCost: number;
    minTime: number;
}

export class SubServiceDto {
    serviceName: string;
    description: string;
    excerpt: string;
}