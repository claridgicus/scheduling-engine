import { TimePref } from '../../index';

export class SlotBook {
    contractorId: string;
    bookingId: string;
    day: number;
    month: number;
    year: number;
    startTime?: number;
    timePref?: TimePref;
    duration: number;
}
