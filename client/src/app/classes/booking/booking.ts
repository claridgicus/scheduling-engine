import { SlotBook } from '../index';

export class Booking {
    id: string;
    bookingSlots: SlotBook[];
}