import { async, inject, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';         import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { AuthenticationService, UserService } from '../../services';
import { AuthGuard } from './auth.guard';

describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService, HttpClient, HttpHandler, AuthenticationService, Router, {provide: APP_BASE_HREF, useValue: '/'}, ActivatedRouteSnapshot, ActivatedRoute],

    });
  });

  it('should ...', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
