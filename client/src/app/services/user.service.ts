import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { LocationDto, LocationPinDto, User } from '../classes/index';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  getCurrentUser() {
    return this.http.get(environment.apiUrl + '/user/');
  }

  getUserById(userId: string) {
    return this.http.get(environment.apiUrl + '/user/role/' + userId);
  }

  getSimpleUserById(userId: string) {
    return this.http.get(environment.apiUrl + '/user/' + userId);
  }

  updateCurrentUser(user: User) {
    return this.http.post(environment.apiUrl + '/user/update', user);
  }

  updateUser(user: any) {
    return this.http.post(environment.apiUrl + '/user/update/user', user );
  }

  resendActivationEmail(email: string){
    return this.http.post(environment.apiUrl + '/user/resend', {email});
  }

  getCurrentUserLocation() {
    return this.http.get(environment.apiUrl + '/user/location');
  }

  updateCurrentUserLocation(location: LocationDto) {
    return this.http.post(environment.apiUrl + '/user/location', location);
  }

  getCurrentUserPin(type: number) {
    return this.http.get(environment.apiUrl + '/user/pin/' + type);
  }

  upsertCurrentUserPin(pin: LocationPinDto) {
    return this.http.post(environment.apiUrl + '/user/pin', pin);
  }

  setRange(range: number) {
    return this.http.post(environment.apiUrl + '/user/range', {range});
  }

}
