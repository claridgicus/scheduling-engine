import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class ServiceService {

  constructor(private http: HttpClient) {
  }

  getServices() {
    return this.http.get(environment.apiUrl + '/service/');
  }

  getServiceById(id) {
    return this.http.get(environment.apiUrl + '/service/' + id);
  }

  setUserService(serviceId: string){
    return this.http.post(environment.apiUrl + '/service/set-service', {serviceId});
  }
}
