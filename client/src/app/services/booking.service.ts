import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import * as uuid from 'uuid';
import { environment } from '../../environments/environment';
import { Cart, CartItem } from '../classes/index';

@Injectable()
export class BookingService {
  
  constructor(private http: HttpClient) {
  }
    
  sendBooking(cart) {
    return this.http.post(environment.apiUrl + '/booking/', cart);
  }

  getBookings() {
    return this.http.get(environment.apiUrl + '/booking/');
  }

  getBookingItemComponents() {
    return this.http.get(environment.apiUrl + '/booking/components/');
  }

}
