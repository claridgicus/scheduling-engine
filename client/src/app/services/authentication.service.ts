import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../classes/user';
import { AuthToken } from './../classes/';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient) {
  }

  login(email: string, password: string) {
    return this.http.post(environment.apiUrl + '/user/login', { email, password });
  }

  magicLogin(authToken: AuthToken) {
    return this.http.post(environment.apiUrl + '/user/magicLogin', authToken);
  }

  updatePassword(user: User, password: string) {
    return this.http.post(environment.apiUrl + '/user/updatePassword', { user, password });
  }

  createUser(user: User) {
    return this.http.post(environment.apiUrl + '/user/create', user );
  }

  createContractor(user: User) {
    return this.http.post(environment.apiUrl + '/user/createContractor', user );
  }

  inviteUser(user: User) {
    return this.http.post(environment.apiUrl + '/user/invite', user );
  }

  removeUser(user: User) {
    return this.http.post(environment.apiUrl + '/user/remove', user);
  }

  resetPassword(email: string) {
    return this.http.post(environment.apiUrl + '/user/resetPassword', { email });
  }

  logout() {
    localStorage.removeItem('currentUser');
  }

  isAuthenticated() {
    return localStorage.getItem('currentUser');
  }

  setToken(token) {
    localStorage.setItem('currentUser', JSON.stringify({
      token
    }));
    return true;
  }

}
