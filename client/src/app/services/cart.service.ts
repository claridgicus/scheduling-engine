import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import * as uuid from 'uuid';
import { environment } from '../../environments/environment';
import { Cart, CartItem } from '../classes/index';

@Injectable()
export class CartService {
  cart: Cart;
  total: number;
  
  constructor(private http: HttpClient) {
    this.initCart();
  }
  
  setCart(cart) {
    localStorage.setItem('cart', JSON.stringify( cart ));
    return true;
  }

  getCart() {
    return JSON.parse(localStorage.getItem('cart'));
  }

  clearCart() {
    localStorage.removeItem('cart');
    this.total = 0;
    this.cart = new Cart();
    this.cart.items = new Array<CartItem>();
    this.setCart(this.cart);
    return this.cart;
  }


  private initCart(){
    this.cart = this.getCart();
    if (!this.cart){
      this.total = 0;
      this.cart = new Cart();
      this.cart.items = new Array<CartItem>();
      this.setCart(this.cart);
    }
    return this.cart;
  }

  public addToCart(cartItem: CartItem){
    cartItem.id = uuid.v4();
    this.cart.items.push(cartItem);
    this.cart.total = this.getCurrentCartTotal(this.cart);
    console.log(cartItem);
    this.setCart(this.cart);
    return cartItem.id;
  }

  public getCurrentCart(){
    this.cart = this.getCart();
    return this.cart;
  }

  public getCurrentCartTotal(cart){
    let total = 0;
    for ( let i = 0; i < cart.items.length; i++){
      total = total + cart.items[i].total;
    }
    return total;
  }

  public removeFromCart(idToDelete){
    this.cart.items = this.cart.items.filter( items => items.id !== idToDelete);
    this.cart.total = this.getCurrentCartTotal(this.cart);
    this.setCart(this.cart);
    return this.cart;
  }

  placeBooking() {
    const cart = this.getCart();
    this.sendBooking(cart).subscribe(
      (data) => { console.log(data); }
    );
    return this.clearCart();
  }

  sendBooking(cart) {
    return this.http.post(environment.apiUrl + '/booking/', cart);
  }


}
