import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookingController } from 'controllers/booking/booking.controller';
import { NotificationController } from 'controllers/notification/notification.controller';
import { BaseEntity } from 'entity/base.entity';
import { Booking } from 'entity/booking.entity';
import { BookingItem } from 'entity/bookingItem.entity';
import { BookingItemComponent } from 'entity/bookingItemComponent.entity';
import { Location } from 'entity/location.entity';
import { LocationPin } from 'entity/locationPin.entity';
import { Notification } from 'entity/notification.entity';
import { Service } from 'entity/service.entity';
import { SubService } from 'entity/subservice.entity';
import { User } from 'entity/user.entity';
import { BookingService } from 'services/booking.service';
import { EmailService } from 'services/email.service';
import { JwtStrategy } from 'services/jwt.strategy';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ServiceController } from './controllers/service/service.controller';
import { UserController } from './controllers/user/user.controller';
import { AuthService } from './services/auth.service';
import { NotificationService } from './services/notification.service';
import { ServiceService } from './services/service.service';
import { UserService } from './services/user.service';
@Module({
  controllers: [AppController, UserController, ServiceController, BookingController, NotificationController],
  imports: [
    TypeOrmModule.forRoot(),
    TypeOrmModule.forFeature([User, Location, LocationPin, Service, SubService, Booking, BookingItem, BookingItemComponent, Notification])
  ],
  providers: [AppService, UserService, AuthService, EmailService, JwtStrategy, ServiceService, BookingService, NotificationService],
  })
export class AppModule {
}
