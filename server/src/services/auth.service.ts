import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcryptjs';
import { User } from 'entity/user.entity';
import * as jwt from 'jsonwebtoken';
import { Repository } from 'typeorm';
import { AuthTokenDto } from './../dtos/auth-token.dto';
import { JwtPayloadDto } from './../dtos/jwt-payload.dto';
import { UserService } from './user.service';
// tslint:disable-next-line:no-var-requires
const config = require('./../config/config.json');

@Injectable()
export class AuthService {
    constructor(@InjectRepository(User) private readonly userRepository: Repository<User>, private readonly userService: UserService){}

    async createToken(id: string, email: string) {
        const user: JwtPayloadDto = { id, email };
        const token = jwt.sign(user, config.secret);
        const AuthToken = new AuthTokenDto();
        AuthToken.token = 'bearer ' + token;
        return AuthToken;
    }

    async validateUser(payload: JwtPayloadDto): Promise<any> {
        return await this.userService.findUserById(payload.id);
    }

    async comparePasswords(loginPassword: string, userPasswordHash: string): Promise<boolean> {
        if (!loginPassword || !userPasswordHash){
            return false;
        }
        const match = await bcrypt.compare(loginPassword, userPasswordHash);
        if (match){
            return true;
        }
        return false;
    }

    async updatePassword(id: string, newPassword: string) {
        const newPasswordHash = await this.hashPassword(newPassword);
        return await this.userRepository.update(id, {password: newPasswordHash});
    }

    private async hashPassword(password: string): Promise<string>{
        const saltRounds = 10;
        return await bcrypt.hash(password, saltRounds);
    }
}
