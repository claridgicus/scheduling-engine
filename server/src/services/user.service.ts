import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as uuid from 'uuid';
import { LocationDto } from './../dtos/location.dto';
import { LocationPinDto } from './../dtos/locationPin.dto';
import { UserDto } from './../dtos/user.dto';
import { Location } from './../entity/location.entity';
import { LocationPin } from './../entity/locationPin.entity';
import { User } from './../entity/user.entity';
import { EmailService } from './email.service';
const config = require('./../config/config.json');

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Location) private readonly locationRepository: Repository<Location>,
        @InjectRepository(LocationPin) private readonly locationPinRepository: Repository<LocationPin>,
        private emailService: EmailService){}

    async createUser(createUser): Promise<boolean> {
        const newUser = new User();
        newUser.resetToken = uuid.v4();
        newUser.resetDate = new Date();
        newUser.firstName = createUser.firstName;
        newUser.lastName = createUser.lastName;
        newUser.email = createUser.email;
        newUser.location = new Location();
        const locationPin = new LocationPin();
        newUser.locationPin = [locationPin];
        const result = await this.userRepository.save(newUser);
        this.emailService.sendAccountConfirmationEmail(result, config.development.clientUi);
        return true;
    }

    async createContractor(createUser): Promise<boolean> {
        const newUser = new User();
        newUser.resetToken = uuid.v4();
        newUser.resetDate = new Date();
        newUser.firstName = createUser.firstName;
        newUser.lastName = createUser.lastName;
        newUser.email = createUser.email;
        newUser.location = new Location();
        newUser.contractor = true;
        const locationPin = new LocationPin();
        const locationPinContractor = new LocationPin();
        locationPinContractor.type = 2;
        newUser.locationPin = [locationPin, locationPinContractor];
        const result = await this.userRepository.save(newUser);
        this.emailService.sendAccountConfirmationEmail(result, config.development.clientUi);
        return true;
    }

    async resendUserActiviation(email): Promise<boolean> {
        const foundUser = await this.userRepository.findOne({
            where: {
                email
            }
        });
        if (foundUser.resetToken){
        this.emailService.sendAccountConfirmationEmail(foundUser, config.development.clientUi);
        return true;
        }
        return false;
    }

    async findUserByEmail(email: string): Promise<User> {
        return await this.userRepository.findOne({ where: { email, active: true } });
    }

    async findUserById(id: string): Promise<User> {
        return await this.userRepository.findOne({ where: { id, active: true }});
    }

    async getUserById(id: string): Promise<UserDto> {
        const user = await this.userRepository.findOne({ where: { id, active: true }, relations: ['location', 'locationPin', 'service'] });
        console.log(user);
        const userDto = new UserDto();
        return userDto.simplify(user);
    }

    async resetPassword(user: User): Promise<boolean> {
        const token = uuid.v4();
        const date = new Date();
        user.resetToken = token;
        user.resetDate = date;
        await this.userRepository.save(user);
        this.emailService.sendPasswordResetEmail(user, config.development.clientUi);
        return true;
    }

    async updateUser(userUpdate: UserDto, userId: string){
        console.log(userUpdate);
        await this.userRepository.save(userUpdate);
        return true;
    }

    async deactivateUserById(id: string){
        return await this.userRepository.update(id, { active: false });
    }

    async deleteToken(id: string){
        return await this.userRepository.update(id, { resetToken: null, resetDate: null });
    }

    async userToAnonUser(user: User): Promise<any> {
        return;
    }

    async upsertUsersLocation(location: LocationDto, userId: string){
        const result = await this.locationRepository.update(userId, location);
        console.log(result);
        return true;
    }

    async upsertPin(pin: LocationPinDto, userId: string){
        let pinEntry = new LocationPin();
        const pinResult = await this.locationPinRepository.findOne({
            where: {
                type: pin.type,
                userId
            }
        });
        if (pinResult){
            pinResult.latitude = pin.lat;
            pinResult.longitude = pin.lng;
            pinEntry = pinResult;
        } else {
            pinEntry.latitude = pin.lat;
            pinEntry.longitude = pin.lng;
            pinEntry.userId = userId;
            pinEntry.type = pin.type;
        }
        const result = await this.locationPinRepository.save(pinEntry);
        return true;
    }

    async getPin(type: number, userId: string){
        return await this.locationPinRepository.findOne({
            where: {
                type,
                userId
            }
        });
    }

    async getCustomerLatLng(userId: string){
        return await this.getPin(1, userId);
    }

    async getContractorLatLng(userId: string){
        return await this.getPin(2, userId);
    }

    async getCustomerById(userId: string){
        const customer = await this.getUserById(userId);
        customer.locationPin = customer.locationPin.filter(x => x.type = 1);
        return customer;
    }
    
    async getContractorsByService(serviceId: string){
        const contractors = await this.userRepository.find({where: { contractor: true, serviceId }, relations: ['locationPin']  });
        for (let i = 0; i < contractors.length; i++ ){
            contractors[i].locationPin = contractors[i].locationPin.filter(x => x.type = 2);
        }
        return contractors;
    }

    async updateRange(range: number, userId: string){
        return await this.userRepository.update(userId, {range});
    }
}
