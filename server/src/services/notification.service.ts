import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as uuid from 'uuid';
import { NotificationDto, sendingType, messageType, BookingIdObj, UserIdObj, ServiceIdObj, sendingType } from './../dtos/notification.dto';
import { Notification } from './../entity/notification.entity';
import { Service } from './../entity/service.entity';
import { SubService } from './../entity/subservice.entity';
import { BookingService } from './booking.service';
import { EmailService } from './email.service';
import { ServiceService } from './service.service';
import { UserService } from './user.service';
const config = require('./../config/config.json');

@Injectable()
export class NotificationService {
    admin: string;
    constructor(
        @InjectRepository(Notification) private readonly notificationRepository: Repository<Notification>,
        private serviceService: ServiceService,
        private emailService: EmailService,
        private bookingService: BookingService,
        private userService: UserService){
            this.admin = 'james@claridgeand.co';
        }

        getNotificationMethodByUser(userId: string){
            // const user = this.userService.findUserById(userId);
            // this function will be handy in the future for SMS
            // return user.notificationType
        }

        bookingPlaced(customerId: string, bookingId: string){
            const bookingObj = new BookingIdObj(bookingId, null, null);
            const userObj = new UserIdObj(customerId, null, customerId);

            const notification = new NotificationDto(2, 4, bookingObj, userObj);
            this.notificationRepository.create(notification);
        }

        contractorInvite(contractorId: string, bookingItemId: string){
            const bookingObj = new BookingIdObj(null, bookingItemId, null);
            const userObj = new UserIdObj(contractorId, contractorId, null);

            const emailContractor = new NotificationDto(2, 2, bookingObj, userObj);
            this.notificationRepository.create(emailContractor);

            const inappContractor = new NotificationDto(1, 2, bookingObj, userObj);
            const result = this.notificationRepository.create(inappContractor);

            // this.emailService.sendContractorJobInvite(result);
        }

        bookingItemTimeout(bookingItemId: string){
            const bookingObj = new BookingIdObj(null, bookingItemId, null);
            const userObj = new UserIdObj(this.admin, null, null);

            const inAppNotification = new NotificationDto(1, 5, bookingObj, userObj);
            this.notificationRepository.create(inAppNotification);
        }

}
