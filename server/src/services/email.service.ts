import { Injectable } from '@nestjs/common';
import { User } from 'entity/user.entity';
const Mailgen = require('mailgen');
const path = require('path');
const Mail = require('./../config/mailconfig.ts');
const config = require('./../config/config.json');

const mailGenerator = new Mailgen({
    product: {
        copyright: 'Copyright © 2018 Royal Freemasons Health. All rights reserved.',
        link: 'https://TODO.com/',
        //  Optional product logo
        logo: 'https://TODO.com/wp-content/uploads/logo.png',
        //  Appears in header & footer of e-mails
        name: 'Royal Freemasons Health',
    },
    theme: {
        // path: path.resolve('src/config/email.html'),
    },
});

@Injectable()
export class EmailService {

    constructor(){}

    async sendAccountConfirmationEmail(user: User, ui: string): Promise<void>{
        const emailContent: any = {
            body: {
                action: {
                    button: {
                        color: '#50be8b',
                        link: 'http://' + ui + '/login?token=' + user.resetToken + '&user=' + user.id + '&redirect=/set',
                        text: 'Set your password',
                    },
                    instructions: 'To get started with Royal Freemasons Health, please click here:',
                },
                bodycopy: 'Welcome to Royal Freemasons Health! We\'re very excited to have you on board.',
                goToAction: {
                    description: 'Check the status of your order in your dashboard',
                    link: 'http://' + ui + '/login?token=' + user.resetToken + '&user=' + user.id + '&redirect=/set',
                    text: 'Verify Account',
                },
                intro: ['Hi ' + user.firstName],
                name: user.firstName,
                outro: '',
                signature: 'Delivered by',
                title: 'Thanks for joining',
            },
        };
        const body = mailGenerator.generate(emailContent);
        const text = mailGenerator.generatePlaintext(emailContent);
        const subject = 'Your new Royal Freemasons Health account is here!';
        const recipient = user.email;
         require('fs').writeFileSync('preview.html', body, 'utf8');
        this.sendMail(body, subject, text, recipient);
    }

    async sendPasswordResetEmail(user: User, ui: string): Promise<void> {
        const emailContent: any = {
            body: {
                action: {
                    button: {
                        color: '#f06543',
                        link: 'http://' + ui + '/login?token=' + user.resetToken + '&user=' + user.id + '&redirect=/set',
                        text: 'Reset your password',
                    },
                    instructions: ['We receieved a request to change your password, if it wasn\'t you don\'t worry about it this link will self destruct in 5 minutes!'],

                },
                bodycopy: '',
                intro: ['Hey ' + user.firstName + ','],
                name: user.firstName,
                outro: '',
                signature: 'Delivered by',
                title: 'Reset your password',
            },
        };
        const body = mailGenerator.generate(emailContent);
        const text = mailGenerator.generatePlaintext(emailContent);
        const subject =  'Reset your Royal Freemasons Health password';
        const recipient = user.email;
         require('fs').writeFileSync('preview.html', body, 'utf8');
        this.sendMail(body, subject, text, recipient);
    }

    private async sendMail(html: string, subject: string, text: string, recipient: string){
        const mail = new Mail({
            html,
            subject,
            text,
            to: recipient,
            errorCallback(err: any) {
                console.log('error: ' + err);
            },
            successCallback(suc: any) {
                console.log('success');
            },
        });
        console.log(mail);
        //  console.log(user);
        mail.send();
    }
}
