import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as uuid from 'uuid';
import { ServiceCreateDto } from './../dtos/service/service-create.dto';
import { UserDto } from './../dtos/user.dto';
import { Service } from './../entity/service.entity';
import { SubService } from './../entity/subservice.entity';
import { User } from './../entity/user.entity';
import { EmailService } from './email.service';
const config = require('./../config/config.json');

@Injectable()
export class ServiceService {
    constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Service) private readonly serviceRepository: Repository<Service>,
        @InjectRepository(SubService) private readonly subServiceRepository: Repository<SubService>,
        private emailService: EmailService){}

        createService(serviceDto: ServiceCreateDto){
            this.serviceRepository.save(serviceDto);
        }

        async getServices(){
            return await this.serviceRepository.find();
        }

        async getServiceById(id){
            return await this.serviceRepository.findOne(id);
        }

        async setUserService(serviceId: string, userId: string){
            const service = await this.serviceRepository.findOne(serviceId);
            await this.userRepository.update(userId, {service});
            return true;
        }
}
