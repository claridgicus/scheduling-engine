import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Booking } from 'entity/booking.entity';
import { BookingItem } from 'entity/bookingItem.entity';
import { BookingItemComponent } from 'entity/bookingItemComponent.entity';
import { Repository } from 'typeorm';
import * as uuid from 'uuid';
import { LocationDto } from './../dtos/location.dto';
import { LocationPinDto } from './../dtos/locationPin.dto';
import { UserDto } from './../dtos/user.dto';
import { Location } from './../entity/location.entity';
import { LocationPin } from './../entity/locationPin.entity';
import { User } from './../entity/user.entity';
import { EmailService } from './email.service';
import { UserService } from './user.service';
const config = require('./../config/config.json');

@Injectable()
export class BookingService {
    constructor(
        @InjectRepository(Booking) private readonly bookingRepository: Repository<Booking>,
        @InjectRepository(BookingItem) private readonly bookingItemRepository: Repository<BookingItem>,
        @InjectRepository(BookingItemComponent) private readonly bookingItemComponentRepository: Repository<BookingItemComponent>,
        private userService: UserService,
        private emailService: EmailService){}

    async createBooking(cartDto, userId: string): Promise<boolean> {
        const user = await this.userService.findUserById(userId);
        const booking = new Booking();
        cartDto.userId = userId;
        cartDto.bookingItems = cartDto.items;
        for (let i = 0; i < cartDto.bookingItems.length; i++){
            cartDto.bookingItems[i].userId = userId;
            cartDto.bookingItems[i].serviceId = cartDto.items[i].service.id;
            cartDto.bookingItems[i].bookingItemComponents = cartDto.items[i].cartItemComponents; 
            for (let j = 0; j < cartDto.items[i].cartItemComponents.length; j++){
                cartDto.items[i].cartItemComponents[j].userId = userId;
            }
        }

        const result = await this.bookingRepository.save(cartDto);
        // this.emailService.sendAccountConfirmationEmail(result, config.development.clientUi);
        for (let i = 0; i < result.bookingItems.length; i++){
            this.poolBookingItem(result.bookingItems[i]);
        }

        return true;
    }

    async poolBookingItem(bookingItem: BookingItem){
        const contractorsInRange = Array<User>();
        // get customer and their customer Pin
        const customer = await this.userService.getCustomerById(bookingItem.userId);
        // get all contractors and their contractor Pin
        const contractors = await this.userService.getContractorsByService(bookingItem.serviceId);

        // filter out all contractors that are not within range of the job
        for (let i = 0; i < contractors.length; i++){
            const range = this.findDistance(customer.locationPin[0], contractors[i].locationPin[0]);
            if (range < contractors[i].range){
                contractorsInRange.push(contractors[i]);
            }
        }

        console.log(contractorsInRange);
        
        if (contractorsInRange.length === 0){
            // notify administrator
            return;
        }


        // call the scheduler check the schedule

        if (contractorsAvailable.length === 0){
            // notify administrator
            return;

        }
        // return a list of contractors

        // send a list of contractors


        return true;
    }



    async getBookings(userId: string): Promise<Array<Booking>> {
        return this.bookingRepository.find({where: {userId}, relations: ['bookingItems', 'bookingItems.bookingItemComponents', 'user'] });
    }

    async getBooking(bookingId: string): Promise<Booking> {
        return this.bookingRepository.findOne({where: {bookingId}, relations: ['bookingItems', 'bookingItems.bookingItemComponents', 'user'] });
    }

    async getBookingItemComponents(userId: string): Promise<Array<BookingItemComponent>> {
        return this.bookingItemComponentRepository.find({where: {userId} });
    }

    findDistance(customerLatLng, contractorLatLng) {
        const radlat1 = Math.PI * customerLatLng.lat / 180;
        const radlat2 = Math.PI * contractorLatLng.lat / 180;
        const theta = customerLatLng.lon - contractorLatLng.lon;
        const radtheta = Math.PI * theta / 180;
        let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515; // to miles
        dist = dist * 1.609344; // miles to km conversion
        return dist;
    }



}
