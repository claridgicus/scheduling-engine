import * as nodemailer from 'nodemailer';
import {environment} from '../config/environment';
const transporter = nodemailer.createTransport({
    auth: {
        pass: environment.sesPass,
        user: environment.sesUser,
    },
    host: environment.sesServer,
    port: 25,
});

module.exports = function(params: any) {
    this.from = environment.sendingEmail;

    this.send = function() {
        const options = {
            from: this.from,
            html: params.html,
            subject: params.subject,
            text: params.message,
            to: params.to,
        };

        // tslint:disable-next-line:only-arrow-functions
        transporter.sendMail(options, function(err, suc) {
            err ? params.errorCallback(err) : params.successCallback(suc);
        });
    };
};