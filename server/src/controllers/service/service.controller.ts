import { BadRequestException, Body, Controller, Get, HttpException, HttpStatus, Param, Post, Req, UseGuards, ValidationPipe} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Service } from 'entity/service.entity';
import { AuthService } from 'services/auth.service';
import { ServiceService } from 'services/service.service';

@Controller('service')
export class ServiceController {

    constructor(private readonly serviceService: ServiceService, private readonly authService: AuthService){}

    //TODO SUPERADMIN ONLY
    @Post('/')
    @UseGuards(AuthGuard('jwt'))
    async createNewService(@Body(new ValidationPipe()) createService: any): Promise<void>{
        return await this.serviceService.createService(createService);
    }


    @Get('/')
    // @UseGuards(AuthGuard('jwt'))
    async getServices(): Promise<Array<Service>>{
        return await this.serviceService.getServices();
    }


    @Get('/:id')
    // @UseGuards(AuthGuard('jwt'))
    async getServiceById(@Param('id') id): Promise<Service>{
        return await this.serviceService.getServiceById(id);
    }

    @Post('/set-service')
    @UseGuards(AuthGuard('jwt'))
    async setService(@Body() body: any, @Req() req): Promise<boolean>{
        return await this.serviceService.setUserService(body.serviceId, req.user.id);
    }

}