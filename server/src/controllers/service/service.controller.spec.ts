import { Test } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './../../services/user.service';
import { CreateUserDto } from './../../dtos/create-user.dto';
import { RoleService } from '../../services/role.service';
import { AuthService } from '../../services/auth.service';
import { getRepositoryToken } from '@nestjs/typeorm'; // Not publicly exposed
import { User } from '../../entity/user.entity';
import { EmailService } from '../../services/email.service';
import { Role } from '../../entity/role.entity';

describe('UserController', () => {
  let userController: UserController;
  let userService: UserService;

  // TODO actually mock useful test data (probably not required in the controller but needed for service injection)
  const mockUserRepository = {
    data: [
      {id: '123123-21412412', firstName: 'Jeff', lastMame: 'ItsMaName', title: 'Producer', public: false, active: true, phone: '214124', email: 'myname@isjeff.com' },
    ],
  };

  const mockRoleRepository = {
    data: [
      {id: '123123-21412412', firstName: 'Jeff', lastMame: 'ItsMaName', title: 'Producer', public: false, active: true, phone: '214124', email: 'myname@isjeff.com' },
    ],
  };
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        UserService,
        RoleService,
        AuthService,
        EmailService,
        {provide: getRepositoryToken(User), useValue: mockUserRepository},
        {provide: getRepositoryToken(Role), useValue: mockRoleRepository}],
    }).compile();

    userService = module.get<UserService>(UserService);
    userController = module.get<UserController>(UserController);

  });
  describe('createAccount', () => {
    it('successful creation should return success', async () => {
      const result = true;
      const fakeUser = new CreateUserDto();

      jest.spyOn(userService, 'createAccount').mockImplementation(() => result);

      expect(await userController.createAccount(fakeUser)).toBe(result);
    });
  });
  describe('inviteUser', () => {
    it('should throw and error if email address is not provided', async () => {

    });
  });
});
