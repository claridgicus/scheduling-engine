import { BadRequestException, Body, Controller, Get, HttpException, HttpStatus, Param, Post, Req, UseGuards, ValidationPipe} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Service } from 'entity/service.entity';
import { AuthService } from 'services/auth.service';
import { BookingService } from 'services/booking.service';
import { Booking } from 'entity/booking.entity';
import { BookingItemComponent } from 'entity/bookingItemComponent.entity';

@Controller('booking')
export class BookingController {

    constructor( private readonly authService: AuthService, private readonly bookingService: BookingService,){}

    @Post('/')
    @UseGuards(AuthGuard('jwt'))
    async createBooking(@Body(new ValidationPipe()) newBooking: any, @Req() req): Promise<boolean>{
        console.log(newBooking);
        return await this.bookingService.createBooking(newBooking, req.user.id);
    }

    @Get('/')
    @UseGuards(AuthGuard('jwt'))
    async getBookings(@Req() req): Promise<Array<Booking>>{
        return await this.bookingService.getBookings(req.user.id);
    }

    @Get('/components')
    @UseGuards(AuthGuard('jwt'))
    async getBookingItemComponents(@Req() req): Promise<Array<BookingItemComponent>>{
        return await this.bookingService.getBookingItemComponents(req.user.id);
    }
}