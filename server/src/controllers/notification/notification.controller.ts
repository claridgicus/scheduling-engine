import { BadRequestException, Body, Controller, Get, HttpException, HttpStatus, Param, Post, Req, UseGuards, ValidationPipe} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Booking } from 'entity/booking.entity';
import { BookingItemComponent } from 'entity/bookingItemComponent.entity';
import { Service } from 'entity/service.entity';
import { AuthService } from 'services/auth.service';
import { BookingService } from 'services/booking.service';
import { NotificationService } from 'services/notification.service';

@Controller('notification')
export class NotificationController {

    constructor( private readonly authService: AuthService, private readonly notificationService: NotificationService){}

}