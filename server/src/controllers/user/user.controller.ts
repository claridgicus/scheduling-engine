import { BadRequestException, Body, Controller, Get, HttpException, HttpStatus, Param, Post, Req, UseGuards, ValidationPipe} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LocationDto } from 'dtos/location.dto';
import { LocationPinDto } from 'dtos/locationPin.dto';
import { LoginDto } from 'dtos/login.dto';
import { MagicLoginDto } from 'dtos/magic-login.dto';
import { UserDto } from 'dtos/user.dto';
import { User } from 'entity/user.entity';
import { AuthService } from 'services/auth.service';
import { UserService } from 'services/user.service';

@Controller('user')
export class UserController {

    constructor(private readonly userService: UserService, private readonly authService: AuthService){}

    @Post('/create')
    async createUser(@Body(new ValidationPipe()) createUser: any): Promise<boolean>{
        return await this.userService.createUser(createUser);
    }

    @Post('/createContractor')
    async createContractor(@Body(new ValidationPipe()) createUser: any): Promise<boolean>{
        return await this.userService.createContractor(createUser);
    }

    @Post('/resend')
    async resendUserActivation(@Body() body: any): Promise<boolean>{
        return await this.userService.resendUserActiviation(body.email);
    }

    @Post('/login')
    async login(@Body(new ValidationPipe()) loginUser: LoginDto){
        const user = await this.userService.findUserByEmail(loginUser.email);
        if (!user){
            throw new HttpException('Authentication failed. No user for this email.', HttpStatus.BAD_REQUEST);
        }

        const passwordResult = await this.authService.comparePasswords(loginUser.password, user.password);
        if (!passwordResult){
            throw new HttpException('Authentication failed. Incorrect email or password provided.', HttpStatus.BAD_REQUEST);
        }

        return await this.authService.createToken(user.id, user.email);
    }

    @Post('/magicLogin')
    async magicLogin(@Body() magicDetails: MagicLoginDto, @Req() req){
        const user: User = await this.userService.findUserById(magicDetails.id);
        if (!user){
            throw new HttpException('Authentication failed. No user for this ID.', HttpStatus.BAD_REQUEST);
        }
        if (!user.resetToken){
            throw new HttpException('Authentication failed. No token for this ID.', HttpStatus.BAD_REQUEST);
        }
        if (user.resetToken !== magicDetails.token){
            throw new HttpException('Authentication failed. Invalid token.', HttpStatus.BAD_REQUEST);
        }
        this.userService.deleteToken(magicDetails.id);
        return await this.authService.createToken(user.id, user.email);
    }

    @Post('/updatePassword')
    @UseGuards(AuthGuard('jwt'))
    async updatePassword(@Body(new ValidationPipe()) updatePasswordDto: any, @Req() req){
        const user = await this.userService.findUserById(req.user.id);
        if (!user){
            throw new HttpException('Authentication failed.', HttpStatus.BAD_REQUEST);
        }
        return await this.authService.updatePassword(user.id, updatePasswordDto.password);

    }

    @Get('/:id')
    @UseGuards(AuthGuard('jwt'))
    async getUserById(@Param('id') id): Promise<UserDto> {
        const user = await this.userService.findUserById(id);
        if (!user){
            throw new HttpException('Unable to find user.', HttpStatus.BAD_REQUEST);
        }
        const userDto = new UserDto();
        userDto.simplify(user);
        return userDto;
    }

    @Get('/')
    @UseGuards(AuthGuard('jwt'))
    async getCurrentUser(@Req() req): Promise<UserDto> {
        const user = await this.userService.getUserById(req.user.id);
        if (!user){
            throw new HttpException('Unable to find user.', HttpStatus.BAD_REQUEST);
        }
        return user;
    }

    @Get('/:email')
    @UseGuards(AuthGuard('jwt'))
    async getUserByEmail(@Param('email') id): Promise<UserDto> {
        // check if is user or is admin
        const user = await this.userService.findUserByEmail(id);
        if (!user){
            throw new HttpException('Unable to find user.', HttpStatus.BAD_REQUEST);
        }
        const userDto = new UserDto();
        userDto.simplify(user);
        return userDto;
    }

    @Post('/resetPassword')
    async resetPassword(@Body(new ValidationPipe()) requestResetDto: any): Promise<boolean>{
        const user = await this.userService.findUserByEmail(requestResetDto.email);
        if (!user){
            throw new HttpException('Invalid user request.', HttpStatus.BAD_REQUEST);
        }
        return await this.userService.resetPassword(user);
    }

    @Post('/update')
    @UseGuards(AuthGuard('jwt'))
    async updateUser(@Body(new ValidationPipe()) userUpdate: UserDto, @Req() req) {
        // check if is user or is admin
        return await this.userService.updateUser(userUpdate, req.user.id);
    }

    @Post('/location')
    @UseGuards(AuthGuard('jwt'))
    async setLocation(@Body(new ValidationPipe()) location: LocationDto, @Req() req) {
        return await this.userService.upsertUsersLocation(location, req.user.id);
    }

    @Get('/location')
    @UseGuards(AuthGuard('jwt'))
    async getLocation(@Req() req) {
        return await this.userService.getUsersLocation(req.user.id);
    }

    @Post('/pin')
    @UseGuards(AuthGuard('jwt'))
    async upsertPin(@Body(new ValidationPipe()) pin: LocationPinDto, @Req() req) {
        return await this.userService.upsertPin(pin, req.user.id);
    }

    @Get('/pin/:type')
    @UseGuards(AuthGuard('jwt'))
    async getPin(@Param('type') type: number, @Req() req) {
        return await this.userService.getPin(type, req.user.id);
    }

    @Post('range')
    @UseGuards(AuthGuard('jwt'))
    async updateRange(@Body(new ValidationPipe()) range: any, @Req() req) {
        return await this.userService.updateRange(range.range, req.user.id);
    }

}