import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BaseEntity } from './base.entity';

@Entity()
export class Notification extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    // message type

    @Column({default: false, nullable: false})
    isSent: boolean;

    @Column({default: false, nullable: false})
    isRead: boolean;

    @Column({nullable: false})
    notificationSendingType: number;

    @Column({nullable: false})
    notificationMessageType: number;

    // user id

    @Column()
    contractorId: string;

    @Column()
    customerId: string;

    @Column({nullable: false})
    recipientId: string;

    // booking id

    @Column()
    bookingId: string;

    @Column()
    bookingItemId: string;

    @Column()
    bookingItemComponentId: string;

}