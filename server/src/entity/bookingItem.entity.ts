import { Column, Entity, PrimaryGeneratedColumn, Generated, OneToOne, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Booking } from './booking.entity';
import { BookingItemComponent } from './bookingItemComponent.entity';
import { Service } from './service.entity';
import { User } from './user.entity';

@Entity()
export class BookingItem extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    @Generated('increment')
    visibleBookingItemId: number;

    // Bookings have many Booking Item

    @Column()
    bookingId: string;

    @ManyToOne(() => Booking, booking => booking.bookingItems, {
        eager: true
    })
    booking: Booking;

    // Booking Items have many Booking Item Components

    @OneToMany(() => BookingItemComponent, bookingItemComponents => bookingItemComponents.bookingItem, {cascade: true})
    bookingItemComponents: BookingItemComponent[];

    // Users have many Booking Item

    @Column()
    userId: string;

    @ManyToOne(() => User, user => user.bookingItems, {
        eager: true
    })
    user: User;

    // Services have many Booking Items
    // But no relations

    // Users have many Booking Item

    @Column()
    serviceId: string;

    @ManyToOne(() => Service, service => service.bookingItems, {
        eager: true
    })
    service: Service;


    // Booking Item Meta

    @Column()
    repetition: number;

    @Column()
    repetitions: number;

    @Column()
    timePref: number;

    @Column()
    startDate: string;

    @Column()
    duration: number;

}



// service: ServiceDto;
// duration: number;
// cartItemComponents: SubCartItem[];
// repetition: number;
// repetitions: RepetitionEnum;
// timePref: number;
// total: number;
// startDate: Date;