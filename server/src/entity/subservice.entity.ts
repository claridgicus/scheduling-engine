import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Service } from './service.entity';
import { User } from './user.entity';

@Entity()
export class SubService extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ length: 128 })
    serviceName: string;

    @ManyToOne(() => Service, service => service.subservices)
    service: Service;
}