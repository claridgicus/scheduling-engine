import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Location } from './location.entity';
import { SubService } from './subservice.entity';
import { BookingItem } from './bookingItem.entity';
import { User } from './user.entity';

@Entity()
export class Service extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ length: 128 })
    serviceName: string;

    @Column()
    description: string;

    @Column()
    excerpt: string;

    @Column({default: 'https://png.icons8.com/metro/1600/washing-machine.png'})
    imageUrl: string;

    @OneToMany(() => SubService, subservice => subservice.service, {cascade: true})
    subservices: SubService[];

    @Column()
    // in cents
    morningCost: number;

    @Column()
    // in cents
    afternoonCost: number;

    @Column()
    // in cents
    publicCost: number;

    @Column()
    // in cents
    weekendCost: number;

    @Column()
    minTime: number;

    @Column({nullable: true})
    note: string;

    @OneToMany(() => BookingItem, bookingItems => bookingItems.service)
    bookingItems: BookingItem[];

    @OneToMany(() => User, user => user.service)
    users: User[];

}