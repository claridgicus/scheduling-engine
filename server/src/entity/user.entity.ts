import { Column, Entity, Generated, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Booking } from './booking.entity';
import { Location } from './location.entity';
import { LocationPin } from './locationPin.entity';
import { BookingItemComponent } from './bookingItemComponent.entity';
import { BookingItem } from './bookingItem.entity';
import { Service } from './service.entity';
import { SubService } from './subservice.entity';

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({default: false, nullable: false})
    contractor: boolean;

    @OneToMany(() => BookingItemComponent, bookingItemComponents => bookingItemComponents.user)
    bookingItemComponents: BookingItemComponent[];

    @OneToMany(() => BookingItem, bookingItems => bookingItems.user)
    bookingItems: BookingItem[];

    @OneToMany(() => Booking, bookings => bookings.user)
    bookings: Booking[];

    @Column({ length: 128 })
    firstName: string;

    @Column({ length: 128 })
    lastName: string;

    @Column('boolean', { default: true })
    active: boolean;

    @Column({ length: 50, nullable: true })
    phone: string | null;

    // Todo enforce email validation with class-validator
    @Column({ length: 128, nullable: false, unique: true })
    email: string;

    @Column({ length: 128, nullable: true })
    password: string | null;

    @Column({nullable: true, default: 10000 })
    range: number;

    @Column({ nullable: true, default: 'https://plus.google.com/_/focus/photos/public/AIbEiAIAAABDCObHuabNzv3afiILdmNhcmRfcGhvdG8qKDlkMTI0ODkxMTRjZTVhODJjNzgzMjBkMTNlNzcxNDM2ZDkwNWQ4ZmIwAZl--9OX64WUnII6qD9LsI0tLQXY?sz=128' })
    avatarUrl: string | null;

    @Column({nullable: true})
    dob: string;

    // Could generate this at database level but seems like something we should do in code
    @Column({ length: 128, nullable: true })
    resetToken: string | null;

    @Column('date', { nullable: true })
    resetDate: Date | null;

    @OneToOne(() => Location, {cascade: true})
    @JoinColumn()
    location: Location;

    @OneToMany(() => LocationPin, locationPin => locationPin.user, {cascade: true})
    locationPin: LocationPin[];

    @OneToMany(() => BookingItemComponent, bookingItemComponent => bookingItemComponent.contractor, {cascade: true})
    contractBookingItemComponents: BookingItemComponent[];

    @ManyToOne(() => Service, service => service.users)
    service: Service;

}