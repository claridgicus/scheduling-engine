import { Column, Entity, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BaseEntity } from './base.entity';
import { User } from './user.entity';

@Entity()
export class LocationPin extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({type: 'decimal', precision: 10, scale: 6, default: 0})
    latitude: number;

    @Column({type: 'decimal', precision: 10, scale: 6, default: 0})
    longitude: number;

    @Column()
    userId: string;

    @ManyToOne(() => User, user => user.locationPin, {
        eager: true
    })
    user: User;

    @Column({default: 1})
    type: number;

}