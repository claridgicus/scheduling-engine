import { Column, Entity, Generated, PrimaryGeneratedColumn, OneToOne, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { BookingItem } from './bookingItem.entity';
import { User } from './user.entity';

@Entity()
export class Booking extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    @Generated('increment')
    visibleBookingId: number;

    // Bookings have many Booking Item

    @OneToMany(() => BookingItem, bookingItems => bookingItems.booking, {cascade: true})
    bookingItems: BookingItem[];

    // A Customer has many Bookings

    @Column({nullable: true})
    userId: string;

    @ManyToOne(() => User, user => user.bookings)
    user: User;

    // Booking Meta


}