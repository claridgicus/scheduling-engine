import { Column, Entity, PrimaryGeneratedColumn, Generated, OneToOne, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { BookingItem } from './bookingItem.entity';
import { User } from './user.entity';

@Entity()
export class BookingItemComponent extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    @Generated('increment')
    visibleBookingItemComponentId: number;

    // Booking Items have many Booking Item Components

    @Column()
    bookingItemId: string;

    @ManyToOne(() => BookingItem, bookingItem => bookingItem.bookingItemComponents, {
        eager: true
    })
    bookingItem: BookingItem;

    // Users have many Booking Item Components

    @Column()
    userId: string;

    @ManyToOne(() => User, user => user.bookingItemComponents, {
        eager: true
    })
    user: User;

    // Contractors have many Booking Item Components

    @Column({nullable: true})
    contractorId: string;

    @ManyToOne(() => User, user => user.contractBookingItemComponents, {
        eager: true
    })
    contractor: User;

    // Booking Item Component Meta

    @Column()
    date: string;

    @Column()
    cost: number;

    @Column({nullable: true})
    timeSlot: number;



}