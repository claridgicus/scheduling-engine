import { Column, Entity, PrimaryGeneratedColumn, OneToOne, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { User } from './user.entity';

@Entity()
export class Location extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({nullable: true})
    userId: string;

    @OneToOne(() => User)
    user: User;

    @Column({nullable: true})
    address1: string;

    @Column({nullable: true})
    address2: string;

    @Column({nullable: true})
    suburb: string;

    @Column({nullable: true})
    postcode: string;

    @Column({nullable: true})
    state: string;

    @Column({nullable: true, default: 'Australia'})
    country: string;

}