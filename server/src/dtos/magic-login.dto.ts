import { IsString } from 'class-validator';
export class MagicLoginDto {
    @IsString() readonly token: string;
    @IsString() readonly id: string;
}
