import { Location } from './../entity/location.entity';
import { LocationPin } from './../entity/locationPin.entity';
import { User } from 'entity/user.entity';

export class UserDto{
    id: string;
    firstName: string;
    lastName: string;
    active: boolean;
    phone: string | null;
    email: string;
    avatarUrl: string | null;
    dob: string;
    location: Location;
    locationPin: LocationPin[];
    contractor: boolean;

    simplify(User: User){
        const userDto = new UserDto();
        userDto.id = User.id;
        userDto.firstName = User.firstName;
        userDto.lastName = User.lastName;
        userDto.active = User.active;
        userDto.phone = User.phone;
        userDto.email = User.email;
        userDto.avatarUrl = User.avatarUrl;
        userDto.dob = User.dob;
        userDto.location = User.location;
        userDto.locationPin = User.locationPin;
        userDto.contractor = User.contractor;
        return userDto;
    }
}

