export class NotificationDto {
   // message type
   isSent: boolean;
   isRead: boolean;
   sendingType: number;
   messageType: number;

   // user id
   contractorId: string;
   customerId: string;
   recipientId: string;

   // booking id
   bookingId: string;
   bookingItemId: string;
   bookingItemComponentId: string;

   constructor(sendingType: sendingType, messageType: messageType, bookingObj: BookingIdObj, userObj: UserIdObj ) {
        this.isSent = false;
        this.isRead = false;
        this.sendingType = sendingType;
        this.messageType = messageType;
        this.bookingId = bookingObj.bookingId;
        this.bookingItemId = bookingObj.bookingItemId;
        this.bookingItemComponentId = bookingObj.bookingItemComponentId;
        this.contractorId = userObj.contractorId;
        this.customerId = userObj.customerId;
        this.recipientId = userObj.recipientId;
   }

}

export enum sendingType {
    inapp = 1,
    email = 2,
    sms = 3
}

export enum messageType {
    invoice = 1,
    contractorInvite = 2,
    contractorAccepted = 3,
    bookingPlaced = 4,
    bookingTimeout = 5,
}

export class BookingIdObj {
    bookingId: string;
    bookingItemId?: string;
    bookingItemComponentId?: string;

    constructor(bookingId, bookingItemId, bookingItemComponentId){
        this.bookingId = bookingId;
        this.bookingItemId = bookingItemId;
        this.bookingItemComponentId = bookingItemComponentId;
    }
}

export class UserIdObj {
    recipientId: string;
    contractorId: string;
    customerId?: string;

    constructor(recipientId, contractorId, customerId){
        this.recipientId = recipientId;
        this.contractorId = contractorId;
        this.customerId = customerId;
    }
}