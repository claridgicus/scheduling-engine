export interface ServiceCreateDto {
    id?: string;
    serviceName: string;
    description: string;
    excerpt: string;
    imageUrl: string;
    subservice: SubServiceCreateDto[];
    morningCost: number;
    afternoonCost: number;
    publicCost: number;
    weekendCost: number;
    minTime: number;
}

export interface SubServiceCreateDto {
    serviceName: string;
    description: string;
    excerpt: string;
}