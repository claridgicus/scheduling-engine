export class LocationPinDto {
    lat: number;
    lng: number;
    type: number; // 1 client 2 contractor
}