export class LocationDto {
    address1: string;
    address2: string;
    suburb: string;
    postcode: string;
    state: string;
    country: string;
    userId: string;
}